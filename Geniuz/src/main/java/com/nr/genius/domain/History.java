package com.nr.genius.domain;

/**
 * Created by Nadil on 23-02-2017.
 */

public class History {

    private String id;
    private String category;
    private String name;
    private String date;
    private String score;

public  History(){

}


    public History(String id, String category, String name, String date, String score) {
        super();
        this.id = id;
        this.category = category;
        this.name = name;
        this.date = date;
        this.score = score;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }


    @Override
    public String toString() {
        return "History{" +
                "id='" + id + '\'' +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", score='" + score + '\'' +
                '}';
    }
}
