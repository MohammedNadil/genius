package com.nr.genius.domain;

public class Questionaire {

	private String Id; // question id
	private String Qs; // question
	private String Ta; // true answer
	private int Pr; // priority
	private int Scr; // score
	private String A, B, C, D;

	public Questionaire() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Questionaire(String id, String qs, String ta, int pr, int scr,
						String a, String b, String c, String d) {
		super();
		Id = id;
		Qs = qs;
		Ta = ta;
		Pr = pr;
		Scr = scr;
		A = a;
		B = b;
		C = c;
		D = d;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getQs() {
		return Qs;
	}

	public void setQs(String qs) {
		Qs = qs;
	}

	public String getTa() {
		return Ta;
	}

	public void setTa(String ta) {
		Ta = ta;
	}

	public int getPr() {
		return Pr;
	}

	public void setPr(int pr) {
		Pr = pr;
	}

	public int getScr() {
		return Scr;
	}

	public void setScr(int scr) {
		Scr = scr;
	}

	public String getA() {
		return A;
	}

	public void setA(String a) {
		A = a;
	}

	public String getB() {
		return B;
	}

	public void setB(String b) {
		B = b;
	}

	public String getC() {
		return C;
	}

	public void setC(String c) {
		C = c;
	}

	public String getD() {
		return D;
	}

	public void setD(String d) {
		D = d;
	}

	@Override
	public String toString() {
		return "Questionaire{" +
				"Id=" + Id +
				", Qs='" + Qs + '\'' +
				", Ta='" + Ta + '\'' +
				", Pr=" + Pr +
				", Scr=" + Scr +
				", A='" + A + '\'' +
				", B='" + B + '\'' +
				", C='" + C + '\'' +
				", D='" + D + '\'' +
				'}';
	}
}