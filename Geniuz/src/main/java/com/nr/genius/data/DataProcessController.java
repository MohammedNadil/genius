package com.nr.genius.data;

import com.nr.genius.SqliteService.SQLiteDAO;
import com.nr.genius.SqliteService.service_impl.Cust_Question;
import com.nr.genius.SqliteService.service_impl.HistoryData;
import com.nr.genius.SqliteService.service_impl.Main_Question;
import com.nr.genius.app.MyApp;
import com.nr.genius.preferance.AppPreferences;

/**
 * Created by Nadil on 21-11-2016.
 */


public class DataProcessController {
    private static volatile DataProcessController instance = null;
    private final AppPreferences appPreference = new AppPreferences();

    private SQLiteDAO sqlInterface = null;

    public static DataProcessController getDataProcessController() {
        if (instance == null) {
            // double check
            synchronized (DataProcessController.class) {
                if (instance == null) {
                    instance = new DataProcessController();
                }
            }
        }
        return instance;
    }
    public AppPreferences getPreference() {
        return appPreference;
    }



    public SQLiteDAO getQuestionTable() {
        sqlInterface= (SQLiteDAO) new Cust_Question(MyApp.getContext());
        return sqlInterface;
    }
    public SQLiteDAO getMainTable() {
        sqlInterface= (SQLiteDAO) new Main_Question(MyApp.getContext());
        return sqlInterface;
    }
    public SQLiteDAO getHistoryTable() {
        sqlInterface= (SQLiteDAO) new HistoryData(MyApp.getContext());
        return sqlInterface;
    }

}
