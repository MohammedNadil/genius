package com.nr.genius.preferance;


import com.nr.genius.constants.Constants;

/**
 * Created by Nadil on 21-11-2016.
 */

public class AppPreferences {
    private final PreferenceWrapper pWrapper = new PreferenceWrapper();


    public void savename(String name) {
        pWrapper.putStringPref(Constants.PREF_NAME, name);
    }

    public String getsavename() {
        return pWrapper.getStringPref(Constants.PREF_NAME, null);
    }


    public void savemute(String sng) {
        pWrapper.putStringPref(Constants.PREF_SOUND, sng);
    }

    public String getmute() {
        return pWrapper.getStringPref(Constants.PREF_SOUND, null);
    }


}


