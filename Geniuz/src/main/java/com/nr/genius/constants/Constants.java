package com.nr.genius.constants;

import android.graphics.Bitmap;

/**
 * Created by Nadil on 21-11-2016.
 */

public class Constants {

    public static final String PREF_NAME = "name";
    public static final String PREF_SOUND = "sng";

    public static final String SOUNDON = "ON";
    public static final String SOUNDOFF = "OFF";


    public static final int UPDATES = 1;
    public static final int DELETE = 2;



    /***
     * Bitmap
     */
    public static Bitmap BITMAP;
    public static String UploadFilePath;
    public static String FilePath;

    /**
     * msg
     ***/
    public static final String MSG_NOT_EMPTY = "Please fill all data";
    public static final String MSG_ADD_USER = "Please add data";
    public static final String MSG_MSG_UPDATE_SUCCESS = "Update Data successfully";
    public static final String MSG_MSG_ADD_SUCCESS = "Added Data successfully";
    public static final String MSG_DELETE_ALL_DATA_SUCCESS = "Successfully delete all data from DB";

    /**
     * for data base
     */
    public static final String DATABASE_NAME = "sqlsample.db";
    public static final int DATABASE_VERSION = 5;


    public static final String MAIN_TABLE = "mainTable";
    public static final String QUES_ID1 = "ques_id1";
    public static final String QUES_Qs1 = "ques_qs1";
    public static final String QUES_Ta1 = "ques_Ta1";
    public static final String QUES_Pr1 = "ques_pr1";
    public static final String QUES_Scr1 = "ques_scr1";
    public static final String QUES_OptA1 = "ques_a1";
    public static final String QUES_OptB1 = "ques_b1";
    public static final String QUES_OptC1 = "ques_c1";
    public static final String QUES_OptD1 = "ques_d1";

    public static final String TABLE_QUESTION = "questionTable";
    public static final String QUES_ID = "ques_id";
    public static final String QUES_Qs = "ques_qs";
    public static final String QUES_Ta = "ques_Ta";
    public static final String QUES_Pr = "ques_pr";
    public static final String QUES_Scr = "ques_scr";
    public static final String QUES_OptA = "ques_a";
    public static final String QUES_OptB = "ques_b";
    public static final String QUES_OptC = "ques_c";
    public static final String QUES_OptD = "ques_d";

    public static final String TABLE_HISTORY = "historytable";
    public static final String HIS_ID = "his_id";
    public static final String HIS_CAT = "his_cat";
    public static final String HIS_NAM = "his_name";
    public static final String HIS_DATE="his_date" ;
    public static final String HIS_SCORE ="his_score" ;



    /***
     * Viewpager headings
     **/
    public static final String HEAD_LOGIN = "Login";

    public static final String HEAD_RECYLER = "Recycler View";

    /***
     * app values
     **/

    public static final String OK = "OK";
    public static final String Cancel = "Cancel";

    public static int Question_Item_Position = 0;


}
