package com.nr.genius.SqliteService;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.nr.genius.constants.Constants;


/**
 * The type Sqlite database service.
 *
 *@author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public abstract class SqliteDatabaseService implements SQLiteDAO {

    /**
     * The Database.
     */
    public SQLiteDatabase database;
    /**
     * The Db helper.
     */
    public SQLiteHelper dbHelper;

    /**
     * Instantiates a new Sqlite database service.
     *
     * @param context the context
     */
    public SqliteDatabaseService(Context context) {
        dbHelper = SQLiteHelper.getInstance(context);
    }

    public SqliteDatabaseService() {

    }

    /**
     * opening database connection
     *
     * @throws SQLException the sql exception
     */

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    /**
     * closing database connection
     */


    @Override
    public void deleteAllDataFromTable(String table_name) {
        open();
        database.delete(table_name,null,null);
        database.close();
    }

    @Override
    public void deleteSelectedItem(String selectedItemID, String tableName) {
        open();
        try {
            database.delete(tableName, Constants.QUES_ID + " = "
                    + selectedItemID, null);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        database.close();
    }


}

