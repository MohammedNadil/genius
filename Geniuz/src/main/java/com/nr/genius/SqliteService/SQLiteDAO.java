package com.nr.genius.SqliteService;

import java.util.ArrayList;

/**
 * SQLiteDAO declare methods relate to QLite process
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 *
 */

public interface SQLiteDAO {

    void add(Object objects);
    ArrayList<Object> getAll();
    void deleteAllDataFromTable(String table_name);
    void deleteSelectedItem(String selectedItemID, String tableName);
    void update(Object objects);

}
