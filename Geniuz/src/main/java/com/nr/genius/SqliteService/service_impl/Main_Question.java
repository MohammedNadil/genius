package com.nr.genius.SqliteService.service_impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.nr.genius.SqliteService.SqliteDatabaseService;
import com.nr.genius.constants.Constants;
import com.nr.genius.domain.Questionaire;

import java.util.ArrayList;

import static com.nr.genius.constants.Constants.QUES_ID1;

/**
 * Created by Nadil on 08-12-2016.
 */

public class Main_Question extends SqliteDatabaseService {


    /**
     * service implement class of user table
     * handling all SQLite process related to type user
     *
     * @author dev.Cobb
     * @version 1.0
     * @since 8 oct 2016
     * <p>
     * <p>
     * <p>
     * <p>
     * /**
     * The constant USER_TABLE.
     */
    public static final String CRETE_MAIN_TABLE = "create table "
            + Constants.MAIN_TABLE + "(" + QUES_ID1
            + " integer primary key autoincrement, "
            + Constants.QUES_Qs1 + " text not null, "
            + Constants.QUES_Ta1 + " text not null, "
            + Constants.QUES_Pr1 + " text not null, "
            + Constants.QUES_Scr1 + " text not null, "
            + Constants.QUES_OptA1 + " text not null, "
            + Constants.QUES_OptB1 + " text not null, "
            + Constants.QUES_OptC1 + " text not null, "
            + Constants.QUES_OptD1 + " text not null );";


    private String[] quesDetails = {
            QUES_ID1,
            Constants.QUES_Qs1,
            Constants.QUES_Ta1,
            Constants.QUES_Pr1,
            Constants.QUES_Scr1,
            Constants.QUES_OptA1,
            Constants.QUES_OptB1,
            Constants.QUES_OptC1,
            Constants.QUES_OptD1};

    /**
     * Instantiates a new User table.
     *
     * @param context the context
     */
    public Main_Question(Context context) {
        super(context);
    }

    public Main_Question() {
        super();
    }


    @Override
    public void add(Object object) {
        open();
         Questionaire questionaire = (Questionaire) object;
        ContentValues values = new ContentValues();

        values.put(Constants.QUES_Qs1, questionaire.getQs());
        values.put(Constants.QUES_Ta1, questionaire.getTa());
        values.put(Constants.QUES_Pr1, questionaire.getPr());
        values.put(Constants.QUES_Scr1, questionaire.getScr());
        values.put(Constants.QUES_OptA1, questionaire.getA());
        values.put(Constants.QUES_OptB1, questionaire.getB());
        values.put(Constants.QUES_OptC1, questionaire.getC());
        values.put(Constants.QUES_OptD1, questionaire.getD());

        long insertId = database.insert(Constants.MAIN_TABLE, null, values);


        database.close();
    }

    @Override
    public ArrayList<Object> getAll() {

        open();

        ArrayList<Object> userList = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * From " + Constants.MAIN_TABLE, null);
        //Cursor cursor = database.query(Constants.MAIN_TABLE, quesDetails,
        //    null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Questionaire questionaire = cursorToQuestionData(cursor);
            userList.add(questionaire);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        database.close();
        return userList;

    }


    @Override
    public void update(Object objects) {

        open();
        Questionaire questionaire = (Questionaire) objects;

        ContentValues values = new ContentValues();


        values.put(Constants.QUES_Qs1, questionaire.getQs());
        values.put(Constants.QUES_Ta1, questionaire.getTa());
        values.put(Constants.QUES_Pr1, questionaire.getPr());
        values.put(Constants.QUES_Scr1, questionaire.getScr());
        values.put(Constants.QUES_OptA1, questionaire.getA());
        values.put(Constants.QUES_OptB1, questionaire.getB());
        values.put(Constants.QUES_OptC1, questionaire.getC());
        values.put(Constants.QUES_OptD1, questionaire.getD());
        database.update(Constants.MAIN_TABLE, values, QUES_ID1 + "=?", new String[]{String.valueOf(questionaire.getId())});

        database.close();

    }


    private Questionaire cursorToQuestionData(Cursor cursor) {
        Questionaire questionData = new Questionaire();

        questionData.setId(cursor.getString(0));
        questionData.setQs(cursor.getString(1));
        questionData.setTa(cursor.getString(2));
        questionData.setPr(cursor.getInt(3));
        questionData.setScr(cursor.getInt(4));
        questionData.setA(cursor.getString(5));
        questionData.setB(cursor.getString(6));
        questionData.setC(cursor.getString(7));
        questionData.setD(cursor.getString(8));

        return questionData;
    }


}