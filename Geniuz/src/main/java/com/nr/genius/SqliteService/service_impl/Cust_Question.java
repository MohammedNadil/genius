package com.nr.genius.SqliteService.service_impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.nr.genius.SqliteService.SqliteDatabaseService;
import com.nr.genius.constants.Constants;
import com.nr.genius.domain.Questionaire;

import java.util.ArrayList;

import static com.nr.genius.constants.Constants.QUES_ID;


/**
 * Created by Nadil on 08-12-2016.
 */

public class Cust_Question extends SqliteDatabaseService {


    /**
     * service implement class of user table
     * handling all SQLite process related to type user
     *
     * @author dev.Cobb
     * @version 1.0
     * @since 8 oct 2016
     * <p>
     * <p>
     * <p>
     * <p>
     * /**
     * The constant USER_TABLE.
     */
    public static final String CRETE_QUESTION_TABLE = "create table "
            + Constants.TABLE_QUESTION + "(" + QUES_ID
            + " integer primary key autoincrement, "
            + Constants.QUES_Qs + " text not null, "
            + Constants.QUES_Ta + " text not null, "
            + Constants.QUES_Pr + " text not null, "
            + Constants.QUES_Scr + " text not null, "
            + Constants.QUES_OptA + " text not null, "
            + Constants.QUES_OptB + " text not null, "
            + Constants.QUES_OptC + " text not null, "
            + Constants.QUES_OptD + " text not null );";


    private String[] quesDetails = {
            QUES_ID,
            Constants.QUES_Qs,
            Constants.QUES_Ta,
            Constants.QUES_Pr,
            Constants.QUES_Scr,
            Constants.QUES_OptA,
            Constants.QUES_OptB,
            Constants.QUES_OptC,
            Constants.QUES_OptD};

    /**
     * Instantiates a new User table.
     *
     * @param context the context
     */
    public Cust_Question(Context context) {
        super(context);
    }

    public Cust_Question() {
        super();
    }


    @Override
    public void add(Object object) {
        open();
        Questionaire questionaire= (Questionaire) object;
        ContentValues values = new ContentValues();
        values.put(Constants.QUES_Qs, questionaire.getQs());
        values.put(Constants.QUES_Ta, questionaire.getTa());
        values.put(Constants.QUES_Pr,1+"");
        values.put(Constants.QUES_Scr,10+"");
        values.put(Constants.QUES_OptA, questionaire.getA());
        values.put(Constants.QUES_OptB, questionaire.getB());
        values.put(Constants.QUES_OptC, questionaire.getC());
        values.put(Constants.QUES_OptD, questionaire.getD());

        long insertId = database.insert(Constants.TABLE_QUESTION, null, values);


        database.close();
    }

    @Override
    public ArrayList<Object> getAll() {

        open();

        ArrayList<Object> userList = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * From "+Constants.TABLE_QUESTION,null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
           Questionaire questionaire = cursorToQuestionData(cursor);
            userList.add(questionaire);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        database.close();
        return userList;

    }



    @Override
    public void update(Object objects) {

        open();
        Questionaire questionaire= (Questionaire) objects;

        ContentValues values = new ContentValues();


        values.put(Constants.QUES_Qs, questionaire.getQs());
        values.put(Constants.QUES_Ta, questionaire.getTa());
        values.put(Constants.QUES_Pr,1+"");
        values.put(Constants.QUES_Scr,10+"");
        values.put(Constants.QUES_OptA, questionaire.getA());
        values.put(Constants.QUES_OptB, questionaire.getB());
        values.put(Constants.QUES_OptC, questionaire.getC());
        values.put(Constants.QUES_OptD, questionaire.getD());
        database.update(Constants.TABLE_QUESTION, values, QUES_ID + "=?", new String[]{String.valueOf(questionaire.getId())});

        database.close();

    }


    private Questionaire cursorToQuestionData(Cursor cursor) {
        Questionaire questionData =  new Questionaire();

        questionData.setId(cursor.getString(0));
        questionData.setQs(cursor.getString(1));
        questionData.setTa(cursor.getString(2));
        questionData.setPr(cursor.getInt(3));
        questionData.setScr(cursor.getInt(4));
        questionData.setA(cursor.getString(5));
        questionData.setB(cursor.getString(6));
        questionData.setC(cursor.getString(7));
        questionData.setD(cursor.getString(8));

        return questionData;
    }


}
