package com.nr.genius.SqliteService;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nr.genius.SqliteService.service_impl.Cust_Question;
import com.nr.genius.SqliteService.service_impl.HistoryData;
import com.nr.genius.SqliteService.service_impl.Main_Question;
import com.nr.genius.constants.Constants;

/**
 * The type Sq lite helper.
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 *
 */
public class SQLiteHelper extends SQLiteOpenHelper {



	private static SQLiteHelper sInstance;


	/**
	 * Instantiates a new Sq lite helper.
	 *
	 * @param context the context
	 */
	public SQLiteHelper(Context context) {
		super(context, Constants.DATABASE_NAME, null,
				Constants.DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	/**
	 * create your SQLiteDatabase tables
	 * you get table creation comments from service implement class of corresponding type
	 *
	 * @param db the SQLiteDatabase
	 */

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(Cust_Question.CRETE_QUESTION_TABLE);

		db.execSQL(Main_Question.CRETE_MAIN_TABLE);

		db.execSQL(HistoryData.CRETE_HISTORY_TABLE);
	}
	/**
	 * Upgrade your SQLiteDatabase tables
	 */

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

		onCreate(db);
	}

	/**
	 * Gets instance.
	 *
	 * @param context the context
	 * @return the instance of SQLiteHelper class
	 */
	public static SQLiteHelper getInstance(Context context) {

		if (sInstance == null) {
			sInstance = new SQLiteHelper(context.getApplicationContext());
		}
		return sInstance;
	}



}
