package com.nr.genius.SqliteService.service_impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.nr.genius.SqliteService.SqliteDatabaseService;
import com.nr.genius.constants.Constants;
import com.nr.genius.domain.History;

import java.util.ArrayList;

import static com.nr.genius.constants.Constants.HIS_ID;

/**
 * Created by Nadil on 08-12-2016.
 */

public class HistoryData extends SqliteDatabaseService {


    public static final String CRETE_HISTORY_TABLE = "create table "
            + Constants.TABLE_HISTORY + "(" + HIS_ID
            + " integer primary key autoincrement, "
            + Constants.HIS_CAT + " text not null, "
            + Constants.HIS_NAM + " text not null, "
            + Constants.HIS_DATE + " text not null, "
            + Constants.HIS_SCORE + " text not null );";


    private String[] quesDetails = {
            HIS_ID,
            Constants.HIS_CAT,
            Constants.HIS_NAM,
            Constants.HIS_DATE,
            Constants.HIS_SCORE,
    };

    /**
     * Instantiates a new User table.
     *
     * @param context the context
     */
    public HistoryData(Context context) {
        super(context);
    }

    public HistoryData() {
        super();
    }


    @Override
    public void add(Object object) {
        open();
        History history = (History) object;

        ContentValues values = new ContentValues();
        values.put(Constants.HIS_CAT, history.getCategory());
        values.put(Constants.HIS_NAM, history.getName());
        values.put(Constants.HIS_DATE, history.getDate());
        values.put(Constants.HIS_SCORE, history.getScore());


        long insertId = database.insert(Constants.TABLE_HISTORY, null, values);


        database.close();
    }

    @Override
    public ArrayList<Object> getAll() {

        open();

        ArrayList<Object> hisorylist = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * From " + Constants.TABLE_HISTORY, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            History history = cursorToHistoryData(cursor);
            hisorylist.add(history);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        database.close();
        return hisorylist;

    }

    public History Single(History history) {
        History historys = new History();
        open();


        Cursor cursor = database.rawQuery("Select * From " + Constants.TABLE_HISTORY + " WHERE " + Constants.HIS_CAT + " =? ", new String[]{history.getCategory()});

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if(cursor.getString(cursor.getColumnIndex(Constants.HIS_CAT)).equals(history.getCategory())){
                historys.setId(cursor.getString(cursor.getColumnIndex(HIS_ID)));
                String his_id = cursor.getString(cursor.getColumnIndex(HIS_ID));
                String his_cat =cursor.getString(cursor.getColumnIndex(Constants.HIS_CAT));
                String his_name = cursor.getString(cursor.getColumnIndex(Constants.HIS_NAM));
                String his_date = cursor.getString(cursor.getColumnIndex(Constants.HIS_DATE));
                String his_scr = cursor.getString(cursor.getColumnIndex(Constants.HIS_SCORE));
                History his = new History(his_id,his_cat,his_name,his_date,his_scr);

            }
            history = cursorToHistoryData(cursor);
            cursor.moveToNext();
        }
        // make sure to close the cursor

        cursor.close();
        database.close();


        return history;
    }

    @Override
    public void update(Object objects) {

        open();
        History history = (History) objects;

        ContentValues values = new ContentValues();

        values.put(Constants.HIS_CAT, history.getCategory());
        values.put(Constants.HIS_NAM, history.getName());
        values.put(Constants.HIS_DATE, history.getDate());
        values.put(Constants.HIS_SCORE, history.getScore());

        database.update(Constants.TABLE_HISTORY, values, HIS_ID + "=?", new String[]{String.valueOf(history.getId())});

        database.close();

    }

    private History cursorToHistoryData(Cursor cursor) {
        History history = new History();


        history.setId(cursor.getString(0));
        history.setCategory(cursor.getString(1));
        history.setName(cursor.getString(2));
        history.setDate(cursor.getString(3));
        history.setScore(cursor.getString(4));

        return history;
    }


}
