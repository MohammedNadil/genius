package com.nr.genius.ui.Dialog;

import android.widget.Toast;

import com.nr.genius.app.MyApp;

/**
 * Created by Nadil on 21-11-2016.
 */

public class ToastHelper {

    public static void show(String message) {
        Toast.makeText(MyApp.getContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    public static void show(String message, int duration) {
        Toast.makeText(MyApp.getContext(), message,
                duration).show();
    }
}
