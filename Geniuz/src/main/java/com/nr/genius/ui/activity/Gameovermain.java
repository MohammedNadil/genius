package com.nr.genius.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.History;

import java.util.Calendar;

import static com.nr.genius.ui.activity.Mode.aaaa;
import static com.nr.genius.ui.activity.Mode.bbbb;
import static com.nr.genius.ui.activity.Mode.cccc;
import static com.nr.genius.ui.activity.Mode.dddd;
import static com.nr.genius.ui.activity.Ques.src;
/**
 * Created by Nadil on 12-01-2017.
 */

public class Gameovermain extends AppCompatActivity {
    TextView scr;
    Button bn;
    String hiscat, hisname, hisdate;
    Animation myAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gameovermain);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);



        scr = (TextView) findViewById(R.id.finalscr);
        bn = (Button) findViewById(R.id.cont);

        scr.setText(src + "");
        final Intent intent1 = new Intent(this, Mode.class);

        bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bn.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent1);
                finish();
            }
        });


        if (aaaa == 1) {
            hiscat = "Science";
        } else if (bbbb == 1) {
            hiscat = "Maths";
        } else if (cccc == 1) {
            hiscat = "GK";
        } else if (dddd == 1) {
            hiscat = "Tech";
        }
        if(DataProcessController.getDataProcessController().getPreference().getsavename()!=null) {
            hisname = DataProcessController.getDataProcessController().getPreference().getsavename();
        }else {
            hisname = "";
        }
        hisdate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
        History history = new History(null, hiscat, hisname, hisdate, String.valueOf(src));
        DataProcessController.getDataProcessController().getHistoryTable().add(history);


    }

    @Override
    public void onBackPressed()
    {

        Intent intent = new Intent(this, Mode.class);
        startActivity(intent);
        finish();
    }
}
