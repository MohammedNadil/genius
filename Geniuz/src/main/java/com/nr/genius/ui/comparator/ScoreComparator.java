package com.nr.genius.ui.comparator;

import com.nr.genius.domain.History;

import java.util.Comparator;

/**
 * Created by Nadil on 23-02-2017.
 */

public class ScoreComparator implements Comparator<History> {


    public ScoreComparator() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public int compare(History cs1, History cs2) {
        return cs1.getScore().compareTo(cs2.getScore());
    }



}
