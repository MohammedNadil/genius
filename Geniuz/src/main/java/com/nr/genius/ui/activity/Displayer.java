package com.nr.genius.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nr.genius.R;
import com.nr.genius.ui.fragments.AddQuestions;

/**
 * Created by Nadil on 28-12-2016.
 */

public class Displayer extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayer);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        AddQuestions fragment = new AddQuestions();

        fm.beginTransaction().add(R.id.display, fragment).commit();
    }

public  void onBackPressed(){

    startActivity(new Intent(this, Custom.class));
    finish();
}
}
