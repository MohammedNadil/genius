package com.nr.genius.ui.adapter.listener;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nr.genius.constants.Constants;
import com.nr.genius.ui.fragments.AddQuestions;
import com.nr.genius.ui.fragments.FragmentUser_ShowDetails_UsingRV;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
 * sections of the app.
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 *
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                // The first section of the app is the most interesting -- it offers
                // a launchpad into the other demonstrations in this example application.
                return new AddQuestions();
            case 1:
               return new FragmentUser_ShowDetails_UsingRV();
            default:
                return new AddQuestions();



        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence head=null;
        switch (position){
            case 0:
                head= Constants.HEAD_LOGIN;
            break;
            case 1:

                head= Constants.HEAD_RECYLER;
                break;
        }
        return head;
    }
}


