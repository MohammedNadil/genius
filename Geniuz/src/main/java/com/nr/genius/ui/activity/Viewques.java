package com.nr.genius.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nr.genius.R;
import com.nr.genius.ui.fragments.FragmentUser_ShowDetails_UsingRV;

/**
 * Created by Nadil on 22-02-2017.
 */

public class Viewques extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayer);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentUser_ShowDetails_UsingRV fragment = new FragmentUser_ShowDetails_UsingRV();
        fm.beginTransaction().add(R.id.display, fragment).commit();

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, Custom.class);
        startActivity(intent);
        finish();
    }
}
