package com.nr.genius.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.adapter.holder.Holder_Ques;
import com.nr.genius.ui.adapter.listener.AdapterDAO;

import java.util.List;

/**
 * Adapter using generate a bunch of views from a single view holder
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public class Adapter_Ques extends RecyclerView.Adapter<Holder_Ques> implements AdapterDAO {

    private List<Questionaire> questionaires;
    private Context context;

    /**
     * Instantiates a new Adapter user.
     *
     * @param questionaires the user list
     */
    public Adapter_Ques(List<Questionaire> questionaires, Context context) {
        this.questionaires = questionaires;
        this.context = context;
    }


    @Override
    public Holder_Ques onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.holder_ques, parent, false);
        return new Holder_Ques(itemView, context, this);

    }

    @Override
    public void onBindViewHolder(Holder_Ques holder, int position) {

        final Questionaire questionaire = questionaires.get(position);
        holder.tv_UserName.setText(questionaire.getQs());
        holder.tv_UserPhone.setText(questionaire.getTa());
        holder.img_UserOption.setTag(questionaire);
    }

    @Override
    public int getItemCount() {
        return questionaires.size();
    }


    @Override
    public void notifyChange(int option) {
        if (Constants.DELETE == option) {
            questionaires.remove(Constants.Question_Item_Position);
        }

        notifyDataSetChanged();
    }
}
