package com.nr.genius.ui.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.adapter.listener.AdapterDAO;

/**
 * Custom dialog for update selected user details
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public class Dialog_Update extends Dialog   {


    // UI references.
    private EditText addquestion;
    private EditText answer;
    private EditText optiona;
    private EditText optionb;
    private EditText optionc;
    private EditText optiond;


    private Questionaire questionaire;
    private AdapterDAO adapterDAO;

    /**
     * Instantiates a new Dialog update.
     *
     * @param context the context
     * @param questionaire
     */
    public Dialog_Update(Context context, Questionaire questionaire, AdapterDAO adapterDAO) {
        super(context);
        this.questionaire=questionaire;
        this.adapterDAO= adapterDAO;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
      //  getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,WindowManager.LayoutParams.WRAP_CONTENT);
        setContentView(R.layout.update);


        addquestion = (EditText) findViewById(R.id.question);
        addquestion.setText(questionaire.getQs());
        answer = (EditText) findViewById(R.id.answer);
        answer.setText(questionaire.getTa());
        optiona = (EditText) findViewById(R.id.optiona);
        optiona.setText(questionaire.getA());
        optionb = (EditText) findViewById(R.id.optionb);
        optionb.setText(questionaire.getB());
        optionc = (EditText) findViewById(R.id.optionc);
        optionc.setText(questionaire.getC());
        optiond = (EditText) findViewById(R.id.optiond);
        optiond.setText(questionaire.getD());



        Button update = (Button) findViewById(R.id.submit);
        update.setText(Constants.OK);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upDateUser();
                dismiss();
            }

        });

        Button cancel = (Button) findViewById(R.id.cancel);
        cancel.setText(Constants.Cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private void upDateUser(){
        String QuestionData = addquestion.getText().toString();
        String AnswerData = answer.getText().toString();
        String OptionA = optiona.getText().toString();
        String OptionB = optionb.getText().toString();
        String OptionC = optionc.getText().toString();
        String OptionD = optiond.getText().toString();


        if (!QuestionData.equalsIgnoreCase("") && !AnswerData.equalsIgnoreCase("")
        && !OptionA.equalsIgnoreCase("") && !OptionB.equalsIgnoreCase("")
        && !OptionC.equalsIgnoreCase("") && !OptionD.equalsIgnoreCase("")) {

            questionaire.setQs(QuestionData);
            questionaire.setTa(AnswerData);
            questionaire.setA(OptionA);
            questionaire.setB(OptionB);
            questionaire.setC(OptionC);
            questionaire.setD(OptionD);
            DataProcessController.getDataProcessController().getQuestionTable().update(questionaire);

            ToastHelper.show(Constants.MSG_MSG_UPDATE_SUCCESS);
            adapterDAO.notifyChange(Constants.UPDATES);
            dismiss();

        } else {
            ToastHelper.show(Constants.MSG_NOT_EMPTY);
        }
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }


}

