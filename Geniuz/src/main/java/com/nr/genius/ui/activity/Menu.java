package com.nr.genius.ui.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.ui.Dialog.ToastHelper;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

import static util.FIle.createDirectory;
import static util.FIle.createFile;


public class Menu extends AppCompatActivity {

    Button button;
    Button button1;
    ImageView volume;
    LinearLayout profile;
    CircleImageView dis;
    ImageView prpic;
    EditText name;
    TextView nam;
    ImageView pic;
    Animation myAnim;

    public static String path;
    final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 102;
    final int MY_PERMISSIONS_REQUEST_CAMERA = 103;




    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.menu);


        prpic = (ImageView) findViewById(R.id.prop);
        button = (Button) findViewById(R.id.playgame);
        button1 = (Button) findViewById(R.id.addquestions);
        profile = (LinearLayout) findViewById(R.id.editpics);
        nam = (TextView) findViewById(R.id.names);
        final TextView ex1 = (TextView) findViewById(R.id.ex);

        final Button b3 = (Button) findViewById(R.id.history);

        final Intent intent = new Intent(this, Mode.class);
        final Intent intent1 = new Intent(this, Custom.class);

        final Intent intent3 = new Intent(this, TabHostMain.class);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);



        nam.setText(DataProcessController.getDataProcessController().getPreference().getsavename());

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((ContextCompat.checkSelfPermission(Menu.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)) {
                    if ((ContextCompat.checkSelfPermission(Menu.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                        try {
                            ProMenu();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    ToastHelper.show("need Permissions");
                    if ((ContextCompat.checkSelfPermission(Menu.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    }
                    if ((ContextCompat.checkSelfPermission(Menu.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
                        }
                    }
                }
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                button.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent);
                finish();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                button1.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent1);
                finish();
            }
        });
        b3.setOnClickListener(new View.OnClickListener()

                              {
                                  @Override
                                  public void onClick(View v) {

                                      b3.startAnimation(myAnim);
                                      myAnim.setDuration(10);
                                      startActivity(intent3);
                                      finish();
                                  }
                              }
        );


        ex1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ex1.startAnimation(myAnim);
                myAnim.setDuration(10);
                ExitDialog();
            }
        });




        changeStatusBarColor();

        loadImageFromFile();


    }




    public void saveImageToFile() throws NullPointerException {
        createDirectory(Environment.getExternalStorageDirectory().getPath().toString() + "/Propic");
        createFile(Environment.getExternalStorageDirectory().getPath().toString() + "/Propic/.image.txt");
        Constants.FilePath = Environment.getExternalStorageDirectory().getPath().toString() + "/Propic/.image.txt";
        try {
            File writeFile = new File(Constants.FilePath);
            FileWriter writer = new FileWriter(writeFile);
            writer.write(Constants.UploadFilePath);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadImageFromFile() throws NullPointerException {
        Constants.FilePath = Environment.getExternalStorageDirectory().getPath().toString() + "/Propic/.image.txt";
        File file = new File(Constants.FilePath);
        if (!file.exists()) {
            ToastHelper.show("Set Name and ProPic");
        } else {
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                String line;
                while ((line = bufferedReader.readLine()) != null) {

                    path = line;
                    Picasso.with(this).load(new File(path)).into(prpic);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    public void onClickSelectImage() {
        startActivityForResult(new Intent(this,
                PickImageActivity.class), 101);
    }

    @Override
    protected void onActivityResult(int request_code, int result_code,
                                    Intent intent) {
        // TODO Auto-generated method stub
        super.onActivityResult(request_code, result_code, intent);
        if (result_code == RESULT_OK) {
            try {
                if (request_code == 101) {
                    if (Constants.UploadFilePath.length() != 0
                            && Constants.UploadFilePath != null) {
                        dis.setImageBitmap(Constants.BITMAP);
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onBackPressed() {

        ExitDialog();

    }

    private void ProMenu() throws FileNotFoundException, NullPointerException {
        final Dialog dialog = new Dialog(this);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.width = 300;
        layoutParams.height = 100;
        dialog.getWindow().setAttributes(layoutParams);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.profile);
        dis = (CircleImageView) dialog.findViewById(R.id.pro);
        //Picasso.with(this).load(new File(path)).into(dis);
        pic = (ImageView) dialog.findViewById(R.id.cam);
        pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClickSelectImage();
            }
        });
        name = (EditText) dialog.findViewById(R.id.proname);
        name.setText(DataProcessController.getDataProcessController().getPreference().getsavename());
        final Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    saveImageToFile();
                    loadImageFromFile();
                    attemptLogin();
                } catch (Exception e) {
                    dialog.dismiss();
                    ToastHelper.show("please set an image");
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private void attemptLogin() {
        String names = name.getText().toString();
        DataProcessController.getDataProcessController().getPreference().savename(names);
        nam.setText(DataProcessController.getDataProcessController().getPreference().getsavename());
    }


    private void ExitDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.quit);
        final Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        final TextView bt_cancel = (TextView) dialog.findViewById(R.id.bt_cancel);
        bt_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                bt_ok.startAnimation(myAnim);
                myAnim.setDuration(10);
                // TODO Auto-generated method stub
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                System.exit(0);
                finish();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}