package com.nr.genius.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.ToastHelper;
import com.nr.genius.ui.activity.MyBounceInterpolator;


/**
 * The type Fragment user login
 * if a user login , save user details  to data base  .
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */

public class AddQuestions extends Fragment {

    // UI references.
    private EditText addquestion;
    private EditText answer;
    private EditText optiona;
    private EditText optionb;
    private EditText optionc;
    private EditText optiond;

    Animation myAnim;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.addquestion, container, false);
       // Bundle args = getArguments();

        addquestion = (EditText) rootView.findViewById(R.id.question);
        answer = (EditText) rootView.findViewById(R.id.answer);
        optiona = (EditText) rootView.findViewById(R.id.optiona);
        optionb = (EditText) rootView.findViewById(R.id.optionb);
        optionc = (EditText) rootView.findViewById(R.id.optionc);
        optiond = (EditText) rootView.findViewById(R.id.optiond);

        myAnim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);



        final Button submitButton = (Button) rootView.findViewById(R.id.submit);
        submitButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                submitButton.startAnimation(myAnim);

                addQues();
            }
        });


        return rootView;
    }


    /**
     * Attempt login.
     */
    protected void addQues() {

        String questionData = addquestion.getText().toString();
        String answerData = answer.getText().toString();
        String optionaData = optiona.getText().toString();
        String optionbData = optionb.getText().toString();
        String optioncData = optionc.getText().toString();
        String optiondData = optiond.getText().toString();


        if (!questionData.equalsIgnoreCase("") && !answerData.equalsIgnoreCase("") && !optionaData.equalsIgnoreCase("") && !optionbData.equalsIgnoreCase("")
                && !optioncData.equalsIgnoreCase("") && !optiondData.equalsIgnoreCase("")) {

            if (answerData.equalsIgnoreCase(optionaData) || answerData.equalsIgnoreCase(optionbData) || answerData.equalsIgnoreCase(optioncData) || answerData.equalsIgnoreCase(optiondData)) {
                Questionaire questionaire = new Questionaire(null, questionData, answerData, 1, 10, optionaData, optionbData, optioncData, optiondData);
                DataProcessController.getDataProcessController().getQuestionTable().add(questionaire);

                ToastHelper.show(Constants.MSG_MSG_ADD_SUCCESS);
                clearData();
            } else {
                ToastHelper.show("Answer Should match any one option");
            }

        } else {
            ToastHelper.show(Constants.MSG_NOT_EMPTY);
        }

    }

    /**
     * clear all field after save into DB
     */
    private void clearData() {
        addquestion.setText("");
        answer.setText("");
        optiona.setText("");
        optionb.setText("");
        optionc.setText("");
        optiond.setText("");
    }


}

