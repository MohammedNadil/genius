package com.nr.genius.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.domain.History;
import com.nr.genius.ui.adapter.holder.Holder_History;
import com.nr.genius.ui.adapter.listener.AdapterDAO;

import java.util.List;

/**
 * Adapter using generate a bunch of views from a single view holder
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public class Adapter_History extends RecyclerView.Adapter<Holder_History> implements AdapterDAO {

    private List<History> histories;
    private Context context;

    /**
     * Instantiates a new Adapter user.
     *
     * @param histories the user list
     */
    public Adapter_History(List<History> histories, Context context) {
        this.histories = histories;
        this.context = context;
    }




    @Override
    public Holder_History onCreateViewHolder(ViewGroup parent, int viewType) {


        View itemView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.activity_tab1, parent, false);
        return new Holder_History(itemView, context, this);

    }

    @Override
    public void onBindViewHolder(Holder_History holder, int position) {


        final History history = histories.get(position);
        holder.hisnames.setText(history.getName());
        holder.hisdates.setText(history.getDate());
        holder.hisscrs.setText(history.getScore());
        holder.hisscrs.setTag(history);
    }



    @Override
    public int getItemCount() {
        return histories.size();
    }


    @Override
    public void notifyChange(int option) {
        if (Constants.DELETE == option) {
           histories.remove(Constants.Question_Item_Position);
        }

        notifyDataSetChanged();
    }
}
