package com.nr.genius.ui.fragments;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.ToastHelper;
import com.nr.genius.ui.activity.MyBounceInterpolator;
import com.nr.genius.ui.adapter.Adapter_Ques;
import com.nr.genius.ui.view.DividerItemDecoration;
import com.nr.genius.ui.view.VerticalLayoutManager;

import java.util.ArrayList;
import java.util.List;


/**
 * The type Fragment  Show Details Using recycle view
 * using show user  details using a android  recycle view
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public class FragmentUser_ShowDetails_UsingRV extends Fragment {
    Button deleteAll;
    TextView data;
    Animation myAnim;

    RecyclerView userRecycler;
    List<Questionaire> userList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_userlist, container, false);
        userRecycler = (RecyclerView) rootView.findViewById(R.id.rv_user_list);
        initializeRecycler(userRecycler);
        setDataToRV();

        myAnim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        data = (TextView) rootView.findViewById(R.id.nodata);
        deleteAll = (Button) rootView.findViewById(R.id.delete);


        if (userList.size() != 0) {
            data.setVisibility(View.INVISIBLE);
            deleteAll.setVisibility(View.VISIBLE);

        } else {
            data.setVisibility(View.VISIBLE);
            deleteAll.setVisibility(View.INVISIBLE);

        }

        deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteAll.startAnimation(myAnim);
                myAnim.setDuration(10);

                ExitDialog();
            }
        });


        return rootView;
    }


    private void ExitDialog() {
        final Dialog dialog = new Dialog(this.getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.deletedata);
        final Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        final TextView bt_cancel = (TextView) dialog.findViewById(R.id.bt_cancel);
        bt_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                bt_ok.startAnimation(myAnim);
                myAnim.setDuration(10);
                DataProcessController.getDataProcessController().getQuestionTable().deleteAllDataFromTable(Constants.TABLE_QUESTION);
                ToastHelper.show(Constants.MSG_DELETE_ALL_DATA_SUCCESS);
                deleteAll.setVisibility(View.INVISIBLE);
                data.setVisibility(View.VISIBLE);
                userRecycler.setAdapter(null);
                dialog.dismiss();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void initializeRecycler(RecyclerView recycler) {
        userRecycler.setHasFixedSize(true);
        userRecycler.setLayoutManager(new VerticalLayoutManager(getContext()));
        userRecycler.addItemDecoration(new DividerItemDecoration(getContext(), null));
    }


    protected void setDataToRV() {

        // get all user details from data  bases
        ArrayList<Object> userDataFromDB = DataProcessController.getDataProcessController().getQuestionTable().getAll();
        userList = new ArrayList<>();

        for (Object object : userDataFromDB) {
            Questionaire user = (Questionaire) object;
            userList.add(user);
        }
        if (userList.size() != 0) {
            // create a ArrayAdapter for generate user name details
            Adapter_Ques adapter_ques = new Adapter_Ques(userList, this.getActivity());
            //set Adapter into Recycler view
            userRecycler.setAdapter(adapter_ques);
        } else {
            ToastHelper.show(Constants.MSG_ADD_USER);
        }

    }


}

