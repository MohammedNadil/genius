package com.nr.genius.ui.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.nr.genius.ui.activity.Menu;
import com.nr.genius.ui.activity.PickImageActivity;


public class PickImageDialog {

	public static void selectImage(final Context context,
			final PickImageActivity response) {
		final CharSequence[] items = { "Take Photo", "Choose from Library",
				"Cancel" };
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Add Photo!");
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				if (items[item].equals("Take Photo")) {
					response.getResult(1);
					dialog.dismiss();
				} else if (items[item].equals("Choose from Library")) {
					response.getResult(2);
					dialog.dismiss();
				} else if (items[item].equals("Cancel")) {
					dialog.dismiss();

					((Activity) context).startActivity(new Intent(context,
							Menu.class));
					((Activity) context).finish();
				}
			}
		});
		builder.show();

	}

}
