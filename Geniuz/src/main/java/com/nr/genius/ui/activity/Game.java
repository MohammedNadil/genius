package com.nr.genius.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.ToastHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nadil on 12-01-2017.
 */

public class Game extends AppCompatActivity {
    TextToSpeech t1;
    Button b1, LF, NEXT;
    TextView qs, opta, optb, optc, optd, scrs, Time, num;
    int i = 1;
    String e = null, stnms;
    Animation myAnim;


    public static int sr = 0, one = 0, two = 0, aaa = 0, bbb = 0, ccc = 0, ddd = 0, qsnum1 = 1;

    List<Questionaire> questionaires = new ArrayList<Questionaire>();
    Questionaire ques = new Questionaire();
    CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);


        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);


        num = (TextView) findViewById(R.id.qsnum);
        b1 = (Button) findViewById(R.id.speech2);
        qs = (TextView) findViewById(R.id.ques);
        opta = (TextView) findViewById(R.id.opa);
        optb = (TextView) findViewById(R.id.opb);
        optc = (TextView) findViewById(R.id.opc);
        optd = (TextView) findViewById(R.id.opd);
        LF = (Button) findViewById(R.id.lifeline);
        scrs = (TextView) findViewById(R.id.urscore);
        NEXT = (Button) findViewById(R.id.nextqs);
        Time = (TextView) findViewById(R.id.timer);

        sr = 0;
        one = 0;
        two = 0;
        i = 0;
        qsnum1 = 1;


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b1.startAnimation(myAnim);
                myAnim.setDuration(10);

                String toSpeak = "Quesion\t\t" + "\n\n" + qs.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "a" + "\n\n" + opta.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "b" + "\n\n" + optb.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "c" + "\n\n" + optc.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "d" + "\n\n" + optd.getText().toString();
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                t1.setPitch(1);
                t1.setSpeechRate(1);
            }
        });

        questionaires = findAllQuestions();
        Collections.shuffle(questionaires);
        displayAllQuestions();
    }

    public void start() {
        Time.setText("60");
        countDownTimer = new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                Time.setText("" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                Time.setText("Time Out");
                gameOver();
            }
        };
        countDownTimer.start();
    }

    public void cancel() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }


    public void gameOver() {
        Intent intent = new Intent(this, Gameover.class);
        startActivity(intent);
    }

    public void Confirm() {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.areyousure);
        final Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        final TextView bt_cancel = (TextView) dialog.findViewById(R.id.bt_cancel);
        final Intent intent = new Intent(this, Custom.class);
        bt_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                cancel();
                startActivity(intent);
                finish();
                dialog.dismiss();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void LifeLine(String i) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.lifeline);
        final Button life1 = (Button) dialog.findViewById(R.id.lf1);
        final Button life2 = (Button) dialog.findViewById(R.id.lf2);
        life1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                int k = 1;
                if (k != one) {
                    if (opta.getText().toString()
                            .equals(ques.getTa())) {
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);

                    } else if (optb.getText().toString()
                            .equals(ques.getTa())) {
                        optd.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);

                    } else if (optc.getText().toString()
                            .equals(ques.getTa())) {
                        opta.setVisibility(View.GONE);
                        optb.setVisibility(View.GONE);

                    } else if (optd.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                    }
                    one = 1;
                    dialog.dismiss();
                } else {
                    ToastHelper.show("Life line Used Once");
                    dialog.dismiss();
                }
            }
        });

        life2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int k = 1;
                if (k != two) {
                    if (opta.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optb.getText().toString()
                            .equals(ques.getTa())) {
                        opta.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optc.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optd.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);
                    }
                    two = 1;
                    dialog.dismiss();
                } else {
                    ToastHelper.show("Life line Used Once");
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    public List<Questionaire> findAllQuestions() {
        List<Object> objects = DataProcessController.getDataProcessController().getQuestionTable().getAll();
        List<Questionaire> questionaires = new ArrayList<Questionaire>();
        for (int i = 0; i < objects.size(); i++) {
            questionaires.add((Questionaire) objects.get(i));
        }
        return questionaires;
    }


    public void displayAllQuestions() {
        start();
        if (i < questionaires.size()) {
            e = questionaires.get(i).getId().toString();
            String QS = questionaires.get(i).getQs().toString();
            String A = questionaires.get(i).getA().toString();
            String B = questionaires.get(i).getB().toString();
            String C = questionaires.get(i).getC().toString();
            String D = questionaires.get(i).getD().toString();

            ArrayList arrayList = new ArrayList();
            arrayList.add(A);
            arrayList.add(B);
            arrayList.add(C);
            arrayList.add(D);
            Collections.shuffle(arrayList);

            String TS = questionaires.get(i).getTa().toString();
            ques = new Questionaire(e, QS, TS, 1, 10, A, B, C, D);

            opta.setVisibility(View.VISIBLE);
            optb.setVisibility(View.VISIBLE);
            optc.setVisibility(View.VISIBLE);
            optd.setVisibility(View.VISIBLE);

            aaa = 0;
            bbb = 0;
            ccc = 0;
            ddd = 0;

            opta.setBackgroundResource(R.drawable.s2btn);
            optb.setBackgroundResource(R.drawable.s2btn);
            optc.setBackgroundResource(R.drawable.s2btn);
            optd.setBackgroundResource(R.drawable.s2btn);




            qs.setText(QS);
            opta.setText((CharSequence) arrayList.get(0));
            optb.setText((CharSequence) arrayList.get(1));
            optc.setText((CharSequence) arrayList.get(2));
            optd.setText((CharSequence) arrayList.get(3));

            i++;

            opta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opta.setBackgroundResource(R.drawable.s3btn);
                    optb.setBackgroundResource(R.drawable.s2btn);
                    optc.setBackgroundResource(R.drawable.s2btn);
                    optd.setBackgroundResource(R.drawable.s2btn);

                    aaa = 1;
                    bbb = 0;
                    ccc = 0;
                    ddd = 0;
                }
            });

            optb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opta.setBackgroundResource(R.drawable.s2btn);
                    optb.setBackgroundResource(R.drawable.s3btn);
                    optc.setBackgroundResource(R.drawable.s2btn);
                    optd.setBackgroundResource(R.drawable.s2btn);

                    aaa = 0;
                    bbb = 1;
                    ccc = 0;
                    ddd = 0;
                }
            });

            optc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opta.setBackgroundResource(R.drawable.s2btn);
                    optb.setBackgroundResource(R.drawable.s2btn);
                    optc.setBackgroundResource(R.drawable.s3btn);
                    optd.setBackgroundResource(R.drawable.s2btn);

                    aaa = 0;
                    bbb = 0;
                    ccc = 1;
                    ddd = 0;
                }
            });
            optd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opta.setBackgroundResource(R.drawable.s2btn);
                    optb.setBackgroundResource(R.drawable.s2btn);
                    optc.setBackgroundResource(R.drawable.s2btn);
                    optd.setBackgroundResource(R.drawable.s3btn);

                    aaa = 0;
                    bbb = 0;
                    ccc = 0;
                    ddd = 1;
                }
            });

            LF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    t1.stop();


                    LifeLine(e);
                }
            });
            NEXT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    t1.stop();
                    if (aaa == 1) {
                        option("a", e);
                    } else if (bbb == 1) {
                        option("b", e);
                    } else if (ccc == 1) {
                        option("c", e);
                    } else if (ddd == 1) {
                        option("d", e);
                    } else {
                        ToastHelper.show("Please select a choice");
                    }
                }
            });
        } else {
            cancel();
            startActivity(new Intent(this, Congrats.class));
            finish();
        }
    }

    public void option(String opt, String f) {
        cancel();
        char option = opt.charAt(0);
        switch (option) {
            case 'a':
                if (ques.getTa().equalsIgnoreCase(opta.getText().toString())) {
                    qsnum1++;
                    stnms = String.valueOf(qsnum1);
                    num.setText(stnms + ")");
                    sr = sr + ques.getScr();

                    displayAllQuestions();
                } else {
                    gameOver();
                }
                break;
            case 'b':
                if (ques.getTa().equalsIgnoreCase(optb.getText().toString())) {
                    qsnum1++;
                    stnms = String.valueOf(qsnum1);
                    num.setText(stnms + ")");
                    sr = sr + ques.getScr();
                    displayAllQuestions();
                } else {
                    gameOver();
                }
                break;
            case 'c':
                if (ques.getTa().equalsIgnoreCase(optc.getText().toString())) {
                    qsnum1++;
                    stnms = String.valueOf(qsnum1);
                    num.setText(stnms + ")");
                    sr = sr + ques.getScr();
                    displayAllQuestions();
                } else {
                    gameOver();
                }

                break;
            case 'd':
                if (ques.getTa().equalsIgnoreCase(optd.getText().toString())) {
                    qsnum1++;
                    stnms = String.valueOf(qsnum1);
                    num.setText(stnms + ")");
                    sr = sr + ques.getScr();
                    displayAllQuestions();
                } else {
                    gameOver();
                }
                break;
        }
        scrs.setText(sr + "");
    }


    @Override
    public void onBackPressed() {

        t1.stop();
        Confirm();

    }

}