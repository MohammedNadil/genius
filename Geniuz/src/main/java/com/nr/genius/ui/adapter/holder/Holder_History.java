package com.nr.genius.ui.adapter.holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.ui.adapter.listener.AdapterDAO;


public class Holder_History extends RecyclerView.ViewHolder  {


    public TextView hisnames;

    public TextView hisdates;

    public TextView hisscrs;


    public Holder_History(View itemView, final Context context , final AdapterDAO adapterDAO) {
        super(itemView);


        hisnames = (TextView) itemView.findViewById(R.id.hisname);
        hisdates = (TextView) itemView.findViewById(R.id.hisdate);
        hisscrs = (TextView) itemView.findViewById(R.id.hisscr);



    }
}
