package com.nr.genius.ui.activity;

import android.support.v7.app.AppCompatActivity;

import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nadil on 22-02-2017.
 */

public class Questiondata extends AppCompatActivity {





    public List<Questionaire> findAllQuestions() {
        List<Object> objects = DataProcessController.getDataProcessController().getMainTable().getAll();
        List<Questionaire> questionaires = new ArrayList<Questionaire>();
        for (int i = 0; i < objects.size(); i++) {
            questionaires.add((Questionaire) objects.get(i));
        }
        return questionaires;
    }

    public List loadQues() {


        List<Questionaire> myArrayList = new ArrayList<Questionaire>();

        /**
         * Science Questions
         */

        myArrayList.add(new Questionaire(null, "In which temperature the density of water is maximum ?", "4 degree C", 1, 10, "100 degree C", "0 degree C", "4 degree C", "273 degree C"));
        myArrayList.add(new Questionaire(null, "Loudness of sound depends upon ______ of the sound wave", "Amplitude", 1, 10, "Frequency", "Wavelength", "Amplitude", "Pitch"));
        myArrayList.add(new Questionaire(null, "The reason behind decrease in temperature in the boiling point of water at high altitudes is: ", "low atmospheric pressure", 1, 10, "high temperature", "low temperature", "high atmospheric pressure", "low atmospheric pressure"));
        myArrayList.add(new Questionaire(null, "The moment of inertia of a body does not depend upon it's : ", "angular velocity", 1, 10, "axis of rotation", "angular velocity", "form of mass", "position of axis of rotation"));
        myArrayList.add(new Questionaire(null, "which method can be applied to determine purity of a metal", "Archimedes principle", 1, 10, "Boyle's las", "Pascal's law", "Archimedes principle", "Newton's law"));
        myArrayList.add(new Questionaire(null, "The instrument used to study the laws of vibrating string is: ", "Sonometer", 1, 10, "Hydrometer", "Hygrometer", "Sonometer", "Electrometer"));
        myArrayList.add(new Questionaire(null, "Which device is used to measure the wavelength of X-rays ? ", "Spectrometer", 1, 10, "Framing Square", "Spectrometer", "Manometer", "SWR meter"));
        myArrayList.add(new Questionaire(null, "The hydraullic press depends upon:", "Pascal's principle", 1, 10, "Coulomb's law", "Pascal's principle", "Boyle's law", "Bernauli's principle"));
        myArrayList.add(new Questionaire(null, "The Newton's first law is also referred to as:", "Law of Inertia", 1, 10, "Law of Inertia", "Law of friction", "Law of moments", "Law of motion"));
        myArrayList.add(new Questionaire(null, "A transformer works with :", "AC", 1, 10, "DC", "AC", "AC & DC", "None of Above"));

        myArrayList.add(new Questionaire(null, "Brass gets discoloured in air because of the presence of which of the following gases in air?", "Hydrogen sulphide", 1, 10, "Oxygen", "Hydrogen sulphide", "Carbon dioxide", "Nitrogen"));
        myArrayList.add(new Questionaire(null, "Which of the following is a non metal that remains liquid at room temperature?", "Bromine", 1, 10, "Phosphorous", "Chlorine", "Bromine", "Helium"));
        myArrayList.add(new Questionaire(null, "Chlorophyll is a naturally occurring chelate compound in which central metal is", "magnesium", 1, 10, "copper", "magnesium", "iron", "calcium"));
        myArrayList.add(new Questionaire(null, "Which of the following is used in pencils?", "Graphite", 1, 10, "Graphite", "Silicon", "Charcoal", "Phosphorous"));
        myArrayList.add(new Questionaire(null, "Which of the following metals forms an amalgam with other metals?", "Mercury", 1, 10, "Tin", "Mercury", "Lead", "Zinc"));
        myArrayList.add(new Questionaire(null, "Chemical formula for water is", "H2O", 1, 10, "NaAlO2", "H2O", "Al2O3", "CaSiO3"));
        myArrayList.add(new Questionaire(null, "The gas usually filled in the electric bulb is", "nitrogen", 1, 10, "nitrogen", "hydrogen", "carbon dioxide", "oxygen"));
        myArrayList.add(new Questionaire(null, "Washing soda is the common name for", "Sodium carbonate", 1, 10, "Sodium carbonate", "Calcium bicarbonate", "Sodium bicarbonate", "Calcium carbonate"));
        myArrayList.add(new Questionaire(null, "Quartz crystals normally used in quartz clocks etc. is chemically", "silicon dioxide", 1, 10, "silicon dioxide", "germanium oxide", "a mixture of germanium oxide and silicon dioxide", "sodium silicate"));
        myArrayList.add(new Questionaire(null, "Which of the gas is not known as green house gas?", "Hydrogen", 1, 10, "Methane", "Nitrous oxide", "Carbon dioxide", "Hydrogen"));

        myArrayList.add(new Questionaire(null, "Bromine is a", "red liquid", 1, 10, "black solid", "red liquid", "colourless gas", "highly inflammable gas"));
        myArrayList.add(new Questionaire(null, "The hardest substance available on earth is", "Diamond", 1, 10, "Gold", "Iron", "Diamond", "Platinum"));
        myArrayList.add(new Questionaire(null, "The variety of coal in which the deposit contains recognisable traces of the original plant material is", "peat", 1, 10, "bitumen", "anthracite", "lignite", "peat"));
        myArrayList.add(new Questionaire(null, "Tetraethyl lead is used as", "petrol additive", 1, 10, "pain killer", "fire extinguisher", "mosquito repellent", "petrol additive"));
        myArrayList.add(new Questionaire(null, "Which of the following is used as a lubricant?", "Graphite", 1, 10, "Graphite", "Silica", "Iron Oxide", "Diamond"));
        myArrayList.add(new Questionaire(null, "The inert gas which is substituted for nitrogen in the air used by deep sea divers for breathing, is", "Helium", 1, 10, "Argon", "Xenon", "Helium", "Krypton"));
        myArrayList.add(new Questionaire(null, "The gases used in different types of welding would include", "oxygen and acetylene", 1, 10, "oxygen and hydrogen", "oxygen, hydrogen, acetylene and nitrogen", "oxygen, acetylene and argon", "oxygen and acetylene"));
        myArrayList.add(new Questionaire(null, "The property of a substance to absorb moisture from the air on exposure is called", "deliquescence", 1, 10, "osmosis", "deliquescence", "efflorescence", "desiccation"));
        myArrayList.add(new Questionaire(null, "In which of the following activities silicon carbide is used?", "cutting very hard substances", 1, 10, "Making cement and glass", "Disinfecting water of ponds", "cutting very hard substances", "Making casts for statues"));
        myArrayList.add(new Questionaire(null, "The average salinity of sea water is", "3.5%", 1, 10, "3%", "3.5%", "2.5%", "2%"));


        /**
         * Maths Questions
         */


        myArrayList.add(new Questionaire(null, "3 4/5 expressed as a decimal is", "3.80", 2, 10, "3.40", "3.45", "3.50", "3.80"));
        myArrayList.add(new Questionaire(null, "3x-4(x+6)=", "-x-24", 2, 10, "x+6", "-x-24", "7x+6", "-7x-24"));
        myArrayList.add(new Questionaire(null, "Which of the following is the Highest Common Factor of 18,24,36 ?", "6", 2, 10, "6", "18", "36", "72"));
        myArrayList.add(new Questionaire(null, "Item bought by a trader for $80 are sold for $100.The profit expressed as a percentage of cost price is", "25%", 2, 10, "2.5%", "20%", "25%", "50%"));
        myArrayList.add(new Questionaire(null, "Given that a and b are integers, which of the following is not neecessarily an integer ?", "b^a", 2, 10, "2a-5b", "a^7", "b^a", "ab"));
        myArrayList.add(new Questionaire(null, "How many subsets does the set {a,b,c,d,e} have ?", "32", 2, 10, "2", "5", "10", "32"));
        myArrayList.add(new Questionaire(null, "which of the following is NOT a prime number", "21", 2, 10, "11", "21", "31", "41"));
        myArrayList.add(new Questionaire(null, "Profit in the partnership of Bess,Bill and Bob are shared in the ratio 1:2:3. If Bill's share of the profit is $300, what is Bob's share ?", "$450", 2, 10, "$150", "$450", "$600", "$900"));
        myArrayList.add(new Questionaire(null, "A man's regular pay is $3 per hour up to 40 hours.Overtime is twice the payment for regular time.If he was paid $168,how many hours over time did he work ?", "8", 2, 10, "8", "16", "28", "48"));
        myArrayList.add(new Questionaire(null, "what is the value of u in the sequence 2,7,14,23,34,u ?", "47", 2, 10, "45", "46", "47", "53"));


        myArrayList.add(new Questionaire(null, "19 + ……. = 42", "23", 2, 10, "23", "61", "0", "42"));
        myArrayList.add(new Questionaire(null, "What is the symbol of pi?", "π", 2, 10, "€", "π", "Ω", "∞"));
        myArrayList.add(new Questionaire(null, "Arrange the numbers in ascending order: 36, 12, 29, 21, 7.", "7, 12, 21, 29, 36 ", 2, 10, "36, 29, 21, 12, 7 ", "36, 29, 7, 21, 12 ", "7, 12, 21, 29, 36 ", "None of these "));
        myArrayList.add(new Questionaire(null, "What is the greatest two digit number?", "99", 2, 10, "10", "90%", "11", "99"));
        myArrayList.add(new Questionaire(null, "How much is 90 – 19?", "71", 2, 10, "71", "109", "81", "None of these"));
        myArrayList.add(new Questionaire(null, "20 is divisible by", "1", 2, 10, "1", "3", "7", "None of these"));
        myArrayList.add(new Questionaire(null, "Find the value of x; if x = (2 × 3) + 11.", "17", 2, 10, "55", "192", "17", "66"));
        myArrayList.add(new Questionaire(null, "What is the smallest three digit number?", "100", 2, 10, "100", "999", "111", "101"));
        myArrayList.add(new Questionaire(null, "How much is 190 – 87 + 16?", "119", 2, 10, "103", "261", "87", "119"));
        myArrayList.add(new Questionaire(null, "What is 1000 × 1 equal to?", "1000", 2, 10, "1", "1000", "0%", "None of these"));

        myArrayList.add(new Questionaire(null, "The average of first 50 natural numbers is", "25.5", 2, 10, "25.30", "25.5", "25.00", "12.25"));
        myArrayList.add(new Questionaire(null, "A fraction which bears the same ratio to 1/27 as 3/11 bear to 5/9 is equal to", "1/55", 2, 10, "1/55", "55", "3/11", "1/11"));
        myArrayList.add(new Questionaire(null, "The number of 3-digit numbers divisible by 6, is", "150", 2, 10, "148", "166", "150", "152"));
        myArrayList.add(new Questionaire(null, "What is 1004 divided by 2?", "502", 2, 10, "52", "502", "520", "5002"));
        myArrayList.add(new Questionaire(null, "A clock strikes once at 1 o’clock, twice at 2 o’clock, thrice at 3 o’clock and so on. How many times will it strike in 24 hours?", "156", 2, 10, "78", "156", "196", "136"));
        myArrayList.add(new Questionaire(null, "125 gallons of a mixture contains 20% water. What amount of additional water should be added such that water content be raised to 25%?", "81/3 gallons", 2, 10, "15/2 gallons.", "17/2 gallons.", "19/2 gallons.", "81/3 gallons"));
        myArrayList.add(new Questionaire(null, "106 × 106 – 94 × 94 = ?", " 2400", 2, 10, "2004", " 2400", "1904", "1906"));
        myArrayList.add(new Questionaire(null, "Which of the following numbers gives 240 when added to its own square?", "15", 2, 10, "15", "16", "18", "20"));
        myArrayList.add(new Questionaire(null, "Evaluation of 83 × 82 × 8-5 is", "1", 2, 10, "1", "0", "8", "None of these."));
        myArrayList.add(new Questionaire(null, "The simplest form of 1.5 : 2.5 is", " 3 : 5", 2, 10, "15 : 25", "0.75 : 1.25", " 3 : 5", "6 : 10"));


        /**
         * GK Questions
         */


        myArrayList.add(new Questionaire(null, "Entomology is the science that studies", "Insects", 3, 10, "The formation of rocks", "Behavior of human beings", "Insects", "None of these"));
        myArrayList.add(new Questionaire(null, "Eritrea, which became the 182nd member of the UN in 1993, is in the continent of", "Africa", 3, 10, "Africa", "Australia", "Europe", "Asia"));
        myArrayList.add(new Questionaire(null, "Garampani sanctuary is located at", "Diphu, Assam", 3, 10, "Gangtok, Sikkim", "Kohima, Nagaland", "Diphu, Assam", "Junagarh, Gujarat"));
        myArrayList.add(new Questionaire(null, "For which of the following disciplines is Nobel Prize awarded?", "All", 3, 10, "Literature, Peace and Economics", "Physics and Chemistry", "Physiology or Medicine", "All"));
        myArrayList.add(new Questionaire(null, "Hitler party which came into power in 1933 is known as", "Nazi Party", 3, 10, "Ku-Klux-Klan", "Democratic Party", "Labour Party", "Nazi Party"));
        myArrayList.add(new Questionaire(null, "FFC stands for", "Film Finance Corporation", 3, 10, "Federation of Football Council", "Film Finance Corporation", "Foreign Finance Corporation", "None of the above"));
        myArrayList.add(new Questionaire(null, "Fastest shorthand writer was", "Dr. G. D. Bist", 3, 10, "Dr. G. D. Bist", "J.M. Tagore", "Khudada Khan", "J.R.D. Tata"));
        myArrayList.add(new Questionaire(null, "Epsom (England) is the place associated with", "Horse racing", 3, 10, "Shooting", "Horse racing", "Polo", "Snooker"));
        myArrayList.add(new Questionaire(null, "Golf player Vijay Singh belongs to which country?", "Fiji", 3, 10, "India", "Fiji", "UK", "USA"));
        myArrayList.add(new Questionaire(null, "First China War was fought between", "China and Britain", 3, 10, "China and Egypt", "China and Greek", "China and Britain", "China and France"));

        myArrayList.add(new Questionaire(null, "For the Olympics and World Tournaments, the dimensions of basketball court are", "28 m x 15 m", 3, 10, "27 m x 16 m", "28 m x 16 m", "26 m x 14 m", "28 m x 15 m"));
        myArrayList.add(new Questionaire(null, "Federation Cup, World Cup, Allywyn International Trophy and Challenge Cup are awarded to winners of", "Volleyball", 3, 10, "Volleyball", "Basketball", "Tennis", "Cricket"));
        myArrayList.add(new Questionaire(null, "Each year World Red Cross and Red Crescent Day is celebrated on", "May 8", 3, 10, "June 8", "June 18", "May 8", "May 18"));
        myArrayList.add(new Questionaire(null, "Famous sculptures depicting art of love built some time in 950 AD - 1050 AD are", "Khajuraho temples", 3, 10, "Konark Temple", "Khajuraho temples", "Mahabalipuram temples", "Sun temple"));
        myArrayList.add(new Questionaire(null, "Guwahati High Court is the judicature of", "All", 3, 10, "Assam", "Nagaland", "Arunachal Pradesh", "All"));
        myArrayList.add(new Questionaire(null, "Fire temple is the place of worship of which of the following religion?", "Zoroastrianism", 3, 10, "Taoism", "Shintoism", "Judaism", "Zoroastrianism"));
        myArrayList.add(new Questionaire(null, "Film and TV institute of India is located at", "Pune", 3, 10, "Pimpri", "Pune", "Perambur", "Rajkot"));
        myArrayList.add(new Questionaire(null, "Georgia, Uzbekistan and Turkmenistan became the members of UNO in", "1992", 3, 10, "1991", "1992", "1993", "1994"));
        myArrayList.add(new Questionaire(null, "Germany signed the Armistice Treaty on ____ and World War I ended", "November 11, 1918", 3, 10, "May 30, 1918", "February 15, 1918", "November 11, 1918", "January 19, 1918"));
        myArrayList.add(new Questionaire(null, "During World War II, when did Germany attack France?", "1940", 3, 10, "1940", "1941", "1942", "1943"));

        myArrayList.add(new Questionaire(null, "The ozone layer restricts", "Ultraviolet radiation", 3, 10, "X-rays and gamma rays", "Visible light", "Infrared radiation", "Ultraviolet radiation"));
        myArrayList.add(new Questionaire(null, "Filaria is caused by", "Mosquito", 3, 10, "Protozoa", "Mosquito", "Virus", "Bacteria"));
        myArrayList.add(new Questionaire(null, "Goa Shipyard Limited (GSL) was established in", "1957", 3, 10, "1955", "1956", "1957", "1958"));
        myArrayList.add(new Questionaire(null, "Coral reefs in India can be found in", "Rameshwaram", 3, 10, "Waltair", "Trivandrum", "the coast of Orissa", "Rameshwaram"));
        myArrayList.add(new Questionaire(null, "For safety, the fuse wire used in the mains for household supply of electricity must be made of metal having", "low melting point", 3, 10, "low specific heat", "high melting point", "high resistance", "low melting point"));
        myArrayList.add(new Questionaire(null, "Golden Temple, Amritsar is India's", "largest Gurdwara", 3, 10, "largest Gurdwara", "oldest Gurudwara", "Both", "None"));
        myArrayList.add(new Questionaire(null, "Heavy Water Project (Talcher) and Fertilizer plant (Paradeep) are famous industries of", "Orissa", 3, 10, "Orissa", "Kerala", "Tamil nadu", "Andhra Pradesh"));
        myArrayList.add(new Questionaire(null, "Hamid Karzai was chosen president of Afghanistan in", "2002", 3, 10, "2000", "2001", "2002", "2003"));
        myArrayList.add(new Questionaire(null, "Durand Cup is associated with the game of", "Football", 3, 10, "Volleyball", "Football", "Cricket", "Hockey"));
        myArrayList.add(new Questionaire(null, "Headquarters of UNO are situated at", "New York, USA", 3, 10, "Paris", "Geneva", "Haque", "New York, USA"));


        /**
         * Technology Questions
         */

        myArrayList.add(new Questionaire(null, "In which decade was the American Institute of Electrical Engineers (AIEE) founded?", "1880s", 4, 10, "1850s", "1880s", "1930s", "1950s"));
        myArrayList.add(new Questionaire(null, "What is part of a database that holds only one type of information?", "Field", 4, 10, "Report", "Field", "Record", "File"));
        myArrayList.add(new Questionaire(null, "'OS' computer abbreviation usually means ?", "Operating System", 4, 10, "Order of Significance", "Open Software", "Operating System", "Optical Sensor"));
        myArrayList.add(new Questionaire(null, "In which decade with the first transatlantic radio broadcast occur?", "1900s", 4, 10, "1850s", "1860s", "1870s", "1900s"));
        myArrayList.add(new Questionaire(null, "'.MOV' extension refers usually to what kind of file?", "Animation/movie file", 4, 10, "Image file", "Animation/movie file", "Audio file", "MS Office document"));
        myArrayList.add(new Questionaire(null, "In which decade was the SPICE simulator introduced?", "1972s", 4, 10, "1950s", "1960s", "1972s", "1980s"));
        myArrayList.add(new Questionaire(null, "Most modern TV's draw power even if turned off. The circuit the power is used in does what function?", "Remote control", 4, 10, "Sound", "Remote control", "Color balance", "High voltage"));
        myArrayList.add(new Questionaire(null, "Which is a type of Electrically-Erasable Programmable Read-Only Memory?", "Flash", 4, 10, "Flash", "Flange", "Fury", "FRAM"));
        myArrayList.add(new Questionaire(null, "'.MPG' extension refers usually to what kind of file?", "Animation/movie file", 4, 10, "WordPerfect Document file", "MS Office document", "Animation/movie file", "Image file"));
        myArrayList.add(new Questionaire(null, "Who is largely responsible for breaking the German Enigma codes, created a test that provided a foundation for artificial intelligence?", "Alan Turing", 4, 10, "Alan Turing", "Jeff Bezos", "George Boole", "Charles Babbage"));

        myArrayList.add(new Questionaire(null, "Made from a variety of materials, such as carbon, which inhibits the flow of current...?", "Resistor", 4, 10, "Choke", "Inductor", "Resistor", "Capacitor"));
        myArrayList.add(new Questionaire(null, "What frequency range is the High Frequency band?", "3 to 30 MHz", 4, 10, "100 kHz", "1 GHz", "30 to 300 MHz", "3 to 30 MHz"));
        myArrayList.add(new Questionaire(null, "The first step to getting output from a laser is to excite an active medium. What is this process called?", "Pumping", 4, 10, "Pumping", "Exciting", "Priming", "Raising"));
        myArrayList.add(new Questionaire(null, "What is the relationship between resistivity r and conductivity s?", "R = 1/s", 4, 10, "R = s2", "R = s", "R > s", "R = 1/s"));
        myArrayList.add(new Questionaire(null, "Which motor is NOT suitable for use as a DC machine?", "Squirrel cage motor", 4, 10, "Permanent magnet motor", "Series motor", "Squirrel cage motor", "Synchronous motor"));
        myArrayList.add(new Questionaire(null, "A given signal's second harmonic is twice the given signal's __________ frequency...?", "Fundamental", 4, 10, "Fourier", "Foundation", "Fundamental", "Field"));
        myArrayList.add(new Questionaire(null, "In which year was MIDI(dress) introduced?", "1983", 4, 10, "1987", "1983", "1973", "1977"));
        myArrayList.add(new Questionaire(null, "When measuring the characteristics of a small-signal amplifier, say for a radio receiver, one might be concerned with its Noise...?", "Figure", 4, 10, "Fundamental", "Fall", "Force", "Figure"));
        myArrayList.add(new Questionaire(null, "DB' computer abbreviation usually means ?", "Database", 4, 10, "Database", "Double Byte", "Data Block", "Driver Boot"));
        myArrayList.add(new Questionaire(null, "The sampling rate, (how many samples per second are stored) for a CD is...?", "44.1 kHz", 4, 10, "48.4 kHz", "22,050 Hz", "44.1 kHz", "48 kHz"));


        myArrayList.add(new Questionaire(null, "After the first photons of light are produced, which process is responsible for amplification of the light?", "Stimulated emission", 4, 10, "Blackbody radiation", "Stimulated emission", "Planck's radiation", "Einstein oscillation"));
        myArrayList.add(new Questionaire(null, "Which is NOT an acceptable method of distributing small power outlets throughout an open plan office area?", "Extension Cords", 4, 10, "Power Poles", "Power Skirting", "Flush Floor Ducting", "Extension Cords"));
        myArrayList.add(new Questionaire(null, "Who co-founded Hotmail in 1996 and then sold the company to Microsoft?", "Sabeer Bhatia", 4, 10, "Shawn Fanning", "Ada Byron Lovelace", "Sabeer Bhatia", "Ray Tomlinson"));
        myArrayList.add(new Questionaire(null, "'.TMP' extension refers usually to what kind of file?", "Temporary file", 4, 10, "Compressed Archive file", "Image file", "Temporary file", "Audio file"));
        myArrayList.add(new Questionaire(null, "The electromagnetic coils on the neck of the picture tube or tubes which pull the electron beam from side to side and up and down are called a...?", "Yoke", 4, 10, "Transformer", "Yoke", "Capacitor", "Diode"));
        myArrayList.add(new Questionaire(null, "In the United States the television broadcast standard is?", "SECAM", 4, 10, "PAL", "NTSC", "SECAM", "RGB"));
        myArrayList.add(new Questionaire(null, "The transformer that develops the high voltage in a home television is commonly called as?", "Flyback", 4, 10, "Tesla coil", "Flyback", "Yoke", "Van de Graaf"));
        myArrayList.add(new Questionaire(null, "Which consists of two plates separated by a dielectric and can store a charge?", "Capacitor", 4, 10, "Inductor", "Capacitor", "Transistor", "Relay"));
        myArrayList.add(new Questionaire(null, "'.JPG' extension refers usually to what kind of file?", "Image file", 4, 10, "System file", "Animation/movie file", "MS Encarta document", "Image file"));
        myArrayList.add(new Questionaire(null, "In what year was the @ chosen for its use in e-mail addresses?", "1972", 4, 10, "1976", "1972", "1980", "1984"));


        return myArrayList;

    }

    public void addToDb(List<Questionaire> questionaires) {
        if (findAllQuestions().size() == 0) {
            for (int i = 0; i < questionaires.size(); i++) {
                DataProcessController.getDataProcessController().getMainTable().add(questionaires.get(i));
            }
        }

    }
}