package com.nr.genius.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.nr.genius.R;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.ToastHelper;

import java.util.ArrayList;
import java.util.List;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

/**
 * Created by Nadil on 09-01-2017.
 */

public class Custom extends AppCompatActivity {

    Animation myAnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custommenu);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-3170216728199381~1785734550");



        AdView mAdView = (AdView) findViewById(R.id.ad_view);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        final Button addq = (Button) findViewById(R.id.add);
        final Button custplay = (Button) findViewById(R.id.customplay);
        final Button viewall = (Button) findViewById(R.id.vtview);

        final List<Questionaire> questionaires = findAllQuestions();

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        final Intent intent = new Intent(this, Displayer.class);
        final Intent intent2 = new Intent(this, Game.class);
        final Intent intent1 = new Intent(this, Viewques.class);



        addq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addq.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent);
                finish();
            }
        });

        custplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                custplay.startAnimation(myAnim);
                myAnim.setDuration(10);
                if (questionaires.size() != 0) {
                    startActivity(intent2);
                } else {
                    ToastHelper.show("Add Questions");
                }
            }
        });

        viewall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewall.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent1);
                finish();
            }
        });
        changeStatusBarColor();


    }

    private List<Questionaire> findAllQuestions() {
        List<Object> objects = DataProcessController.getDataProcessController().getQuestionTable().getAll();
        List<Questionaire> questionaires = new ArrayList<Questionaire>();
        for (int j = 0; j < objects.size(); j++) {
            questionaires.add((Questionaire) objects.get(j));
        }
        return questionaires;
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, Menu.class));
        finish();
    }
}