package com.nr.genius.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.nr.genius.R;
import com.nr.genius.ui.fragments.Tab1;
import com.nr.genius.ui.fragments.Tab2;
import com.nr.genius.ui.fragments.Tab3;
import com.nr.genius.ui.fragments.Tab4;

/**
 * Created by Nadil on 22-02-2017.
 */

public class TabHostMain extends FragmentActivity {


    FragmentTabHost mTabHost;

    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.history);


        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);


        mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("tab1").setIndicator("Science"),
                Tab1.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab2").setIndicator("Maths"),
                Tab2.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab3").setIndicator("Gk"),
                Tab3.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("tab4").setIndicator("Tech"),
                Tab4.class, null);
        mTabHost.getTabWidget()
                .setBackgroundColor(Color.parseColor("#ffffff"));


        changeStatusBarColor();


    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

    }


    @Override
    public void onBackPressed() {


        startActivity(new Intent(this, Menu.class));
        finish();
    }
}
