package com.nr.genius.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;


public class Splash2 extends Questiondata {
    Intent intent;

    private static final int PROGRESS = 0x1;
    TextView t1;
    TextView t2;
    private ProgressBar mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash2);

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Questions");
        myRef.setValue(loadQues());


        final Animation in = new AlphaAnimation(0.0f, 1.0f);
        in.setDuration(3000);

        Typeface tf = Typeface.createFromAsset(getAssets(),
                "font/Pacifico.ttf");
        t1 = (TextView) findViewById(R.id.text1);
        t1.setTypeface(tf);
        t2 = (TextView) findViewById(R.id.text2);
        t2.setTypeface(tf);

        t1.startAnimation(in);
        t2.startAnimation(in);


        intent = new Intent(this, Menu.class);
        mProgress = (ProgressBar) findViewById(R.id.progress_bar);


        final int totalProgressTime = 100;

        final Thread t = new Thread() {

            @Override
            public void run() {

                int jumpTime = 0;
                while (jumpTime < totalProgressTime) {
                    try {
                        sleep(2000);
                        jumpTime = jumpTime + 50;
                        mProgress.setProgress(jumpTime);

                        addToDb(loadQues());
                        if (jumpTime == totalProgressTime) {


                            startActivity(intent);
                            finish();
                        }

                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }
        };
        t.start();

        DataProcessController.getDataProcessController().getPreference().savemute(Constants.SOUNDON);
        changeStatusBarColor();
    }

    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }


    @Override
    public void onBackPressed() {


        finish();
    }


}