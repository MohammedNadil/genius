package com.nr.genius.ui.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.History;
import com.nr.genius.ui.Dialog.ToastHelper;
import com.nr.genius.ui.activity.MyBounceInterpolator;
import com.nr.genius.ui.adapter.Adapter_History;
import com.nr.genius.ui.comparator.ScoreComparator;
import com.nr.genius.ui.view.DividerItemDecoration;
import com.nr.genius.ui.view.VerticalLayoutManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tab2 extends Fragment {
    RecyclerView userRecycler;

    Animation myAnim;
    List<History> histories;
    TextView his;
    Button clearh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View V = inflater.inflate(R.layout.fragment_history, container, false);
        userRecycler = (RecyclerView) V.findViewById(R.id.history_list);
        myAnim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        his = (TextView) V.findViewById(R.id.nohis);
        clearh = (Button) V.findViewById(R.id.clear);
        initializeRecycler(userRecycler);
        setDataToRV();

        if (histories.size() != 0) {
            his.setVisibility(View.INVISIBLE);
            clearh.setVisibility(View.VISIBLE);

        } else {
            his.setVisibility(View.VISIBLE);
            clearh.setVisibility(View.INVISIBLE);
        }


        clearh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearh.startAnimation(myAnim);
                myAnim.setDuration(10);

                ExitDialog();

            }
        });

        return V;
    }
    private void ExitDialog() {
        final Dialog dialog = new Dialog(this.getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.clearhistory);
        final Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        final TextView bt_cancel = (TextView) dialog.findViewById(R.id.bt_cancel);
        bt_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                bt_ok.startAnimation(myAnim);
                myAnim.setDuration(10);

                DataProcessController.getDataProcessController().getHistoryTable().deleteAllDataFromTable(Constants.TABLE_HISTORY);
                ToastHelper.show("History Cleared");
                clearh.setVisibility(View.INVISIBLE);
                his.setVisibility(View.VISIBLE);
                userRecycler.setAdapter(null);

                dialog.dismiss();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void initializeRecycler(RecyclerView recycler) {
        userRecycler.setHasFixedSize(true);
        userRecycler.setLayoutManager(new VerticalLayoutManager(getContext()));
        userRecycler.addItemDecoration(new DividerItemDecoration(getContext(), null));
    }


    protected void setDataToRV() {


        // get all user details from data  bases
        ArrayList<Object> userDataFromDB = DataProcessController.getDataProcessController().getHistoryTable().getAll();
        histories = new ArrayList<>();

        for (Object object : userDataFromDB) {
            History history = (History) object;
            String a = "Maths";
            if (history.getCategory().equalsIgnoreCase(a)) {
                histories.add(history);

                Collections.sort(histories, new ScoreComparator());
                Collections.reverse(histories);

                if (histories.size() != 0) {
                    // create a ArrayAdapter for generate user name details
                    Adapter_History adapter_history = new Adapter_History(histories, this.getActivity());
                    //set Adapter into Recycler view
                    userRecycler.setAdapter(adapter_history);

                } else {
                    ToastHelper.show("No History");
                }
            }
        }

    }
}

