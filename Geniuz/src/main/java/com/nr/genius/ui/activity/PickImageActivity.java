package com.nr.genius.ui.activity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;

import com.nr.genius.constants.Constants;
import com.nr.genius.ui.Dialog.PickImageDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PickImageActivity extends Activity implements com.nr.genius.listener.OnResult {
	private final int REQUEST_CAMERA = 101;
	private final int REQUEST_SELECT_FILE = 102;
	Intent intent;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	intent = getIntent();
	PickImageDialog.selectImage(PickImageActivity.this, this);
}
	
	@SuppressLint("NewApi")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			try {
				if (requestCode == REQUEST_CAMERA) {

					Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
					ByteArrayOutputStream bytes = new ByteArrayOutputStream();
					thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
					
					//create file object that used to create file into SD card
					File destination = new File(
							Environment.getExternalStorageDirectory(),
							System.currentTimeMillis() + ".jpg");
					
					
					Constants.BITMAP = thumbnail;
					Constants.UploadFilePath = destination
							.getAbsolutePath();
					
					saveInToSdCard(destination, bytes);
					
				} else if (requestCode == REQUEST_SELECT_FILE) {
					Uri selectedImageUri = data.getData();
					String[] projection = { MediaColumns.DATA };
					CursorLoader cursorLoader = new CursorLoader(this,
							selectedImageUri, projection, null, null, null);
					Cursor cursor = cursorLoader.loadInBackground();
					int column_index = cursor
							.getColumnIndexOrThrow(MediaColumns.DATA);
					cursor.moveToFirst();
					String selectedImagePath = cursor.getString(column_index);

					BitmapFactory.Options options = new BitmapFactory.Options();
					options.inJustDecodeBounds = true;
					BitmapFactory.decodeFile(selectedImagePath, options);
					final int REQUIRED_SIZE = 200;
					int scale = 1;
					while (options.outWidth / scale / 2 >= REQUIRED_SIZE
							&& options.outHeight / scale / 2 >= REQUIRED_SIZE)
						scale *= 2;
					options.inSampleSize = scale;
					options.inJustDecodeBounds = false;
					Constants.BITMAP = BitmapFactory.decodeFile(
							selectedImagePath, options);
					Constants.UploadFilePath = selectedImagePath;

				}

				setResult(Activity.RESULT_OK, intent);

				finish();
				
			} catch (Exception e) {
				Log.e("Error in get image", e.toString());
			}
		} else {
			finish();
		}

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Constants.BITMAP = null;
		Constants.UploadFilePath = null;
		startActivity(new Intent(PickImageActivity.this,Menu.class));
		finish();
	
	}

	@Override
	public void getResult(Object object) {
		int option = (Integer) object;
		if (option == 1) {

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, REQUEST_CAMERA);
			

		} else if (option == 2) {

			Intent intent = new Intent(
					Intent.ACTION_PICK,
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(Intent.createChooser(intent, "Select File"),
					REQUEST_SELECT_FILE);
			

		}

	}
	private void saveInToSdCard(File destination,	ByteArrayOutputStream bytes) {
		// TODO Auto-generated method stub
		FileOutputStream fo;
		try {
			destination.createNewFile();
			fo = new FileOutputStream(destination);
			fo.write(bytes.toByteArray());
			fo.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
