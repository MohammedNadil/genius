package com.nr.genius.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.nr.genius.R;

import static com.nr.genius.ui.activity.Game.sr;


/**
 * Created by Nadil on 15-01-2017.
 */

public class Congrats extends AppCompatActivity {

    TextView scr;
    Button bn;
    Animation myAnim;



    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.congrats);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);




        scr= (TextView) findViewById(R.id.finalscr);
        bn= (Button) findViewById(R.id.cont);

        final Intent intent1= new Intent(this,Custom.class);
        scr.setText(sr+"");

        bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bn.startAnimation(myAnim);
                myAnim.setDuration(10);
                startActivity(intent1);
                finish();
            }
        });
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, Custom.class);
        startActivity(intent);
        finish();
    }
}
