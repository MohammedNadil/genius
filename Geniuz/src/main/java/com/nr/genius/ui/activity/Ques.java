package com.nr.genius.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.nr.genius.R;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.ToastHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import static com.nr.genius.R.drawable.a;
import static com.nr.genius.ui.activity.Mode.aaaa;
import static com.nr.genius.ui.activity.Mode.bbbb;
import static com.nr.genius.ui.activity.Mode.cccc;
import static com.nr.genius.ui.activity.Mode.dddd;


/**
 * Created by Nadil on 18-01-2017.
 */

public class Ques extends AppCompatActivity {
    TextToSpeech t1;
    TextView qs, opta, optb, optc, optd, num, Time, scrs;
    Button b1, LF, NEXT;
    Animation myAnim;
    Questionaire abbbb;

    private static final String TAG = "CHECK";
    List<Questionaire> questionaires1 = new ArrayList<Questionaire>();
    List<Questionaire> questionaires2 = new ArrayList<Questionaire>();
    List<Questionaire> questionaires3 = new ArrayList<Questionaire>();
    List<Questionaire> questionaires4 = new ArrayList<Questionaire>();
    Questionaire questionairess;

    Questionaire ques = new Questionaire();
    List<Questionaire> questionaires = new ArrayList<Questionaire>();
    ArrayList arrayList;

    int i, j, k, l;
    String e = null, stnms, QS, A, B, C, D;
    CountDownTimer countDownTimer;

    public static int src, one = 0, two = 0, aa = 0, bb = 0, cc = 0, dd = 0, maths = 0, gk = 0, tech = 0, qsnum = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainques);


        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        b1 = (Button) findViewById(R.id.speech);
        num = (TextView) findViewById(R.id.qsnum);

        qs = (TextView) findViewById(R.id.quesmain);
        opta = (TextView) findViewById(R.id.opamain);
        optb = (TextView) findViewById(R.id.opbmain);
        optc = (TextView) findViewById(R.id.opcmain);
        optd = (TextView) findViewById(R.id.opdmain);

        LF = (Button) findViewById(R.id.lifeline);
        NEXT = (Button) findViewById(R.id.nextqsmain);
        scrs = (TextView) findViewById(R.id.urscore);
        Time = (TextView) findViewById(R.id.time);

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b1.startAnimation(myAnim);
                myAnim.setDuration(10);

                String toSpeak = "Quesion\t\t" + "\n\n" + qs.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "a" + "\n\n" + opta.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "b" + "\n\n" + optb.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "c" + "\n\n" + optc.getText().toString() + "\n\n"
                        + "option\t\t" + "\n\n" + "d" + "\n\n" + optd.getText().toString();


                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                t1.setPitch(1);
                t1.setSpeechRate(1);
            }
        });

        qsnum = 1;
        i = 0;
        j = 0;
        k = 0;
        l = 0;
        src = 0;
        one = 0;
        two = 0;


        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Questions");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long value = dataSnapshot.getChildrenCount();
                GenericTypeIndicator<List<Questionaire>> genericTypeIndicator = new GenericTypeIndicator<List<Questionaire>>() {
                };
                questionaires = dataSnapshot.getValue(genericTypeIndicator);


//                for (int p = 0; p < questionaires.size(); p++) {
//                    if (questionaires.get(p).getPr() == 1) {
//                        questionairess = questionaires.get(p);
//
//
//                        Log.d(TAG, " Splitted Value : " + questionairess);
//                    }
//                    // String tata= String.valueOf(questionaires.get(p));
//
//                }
                questionaires1 = findAllScienceQuestions();
                Collections.shuffle(questionaires1);
                questionaires2 = findAllmathsQuestions();
                Collections.shuffle(questionaires2);
                questionaires3 = findAllgkQuestions();
                Collections.shuffle(questionaires3);
                questionaires4 = findAllTechQuestions();
                Collections.shuffle(questionaires4);
                qstn();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


    }


    public void onPause() {
        if (t1 != null) {
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }

    public void qstn() {
        if (aaaa == 1) {
            displayAllscienceQuestions();
        } else if (bbbb == 1) {
            displayAllmathsQuestions();
        } else if (cccc == 1) {
            displayAllgkQuestions();
        } else if (dddd == 1) {
            displayAllTechQuestions();
        }

    }

    public void start() {
        Time.setText("60");
        countDownTimer = new CountDownTimer(60 * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Time.setText("" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                Time.setText("Time Out");
                gameOver();
            }
        };
        countDownTimer.start();
    }

    public void cancel() {

        if (countDownTimer != null) {

            countDownTimer.cancel();
            countDownTimer = null;

        }
    }

    public List<Questionaire> findAllQuestions() {

        // List<Object> objects = DataProcessController.getDataProcessController().getMainTable().getAll();

        List<Questionaire> questionaire = new ArrayList<Questionaire>();
        for (int i = 0; i < questionaires.size(); i++) {
            questionaire.add(questionaires.get(i));
        }

        return questionaire;
    }

    public List<Questionaire> findAllScienceQuestions() {
        List<Questionaire> questionaires22 = findAllQuestions();
        List<Questionaire> questionaires33 = new ArrayList<Questionaire>();
        for (int i = 0; i < questionaires22.size(); i++) {
            if (questionaires22.get(i).getPr() == 1) {
                questionaires33.add(questionaires22.get(i));
            }
            ;
        }
        return questionaires33;
    }

    public List<Questionaire> findAllmathsQuestions() {
        List<Questionaire> questionaires22 = findAllQuestions();
        List<Questionaire> questionaires33 = new ArrayList<Questionaire>();
        for (int i = 0; i < questionaires22.size(); i++) {
            if (questionaires22.get(i).getPr() == 2) {
                questionaires33.add(questionaires22.get(i));
            }
            ;
        }
        return questionaires33;
    }

    public List<Questionaire> findAllgkQuestions() {
        List<Questionaire> questionaires22 = findAllQuestions();
        List<Questionaire> questionaires33 = new ArrayList<Questionaire>();
        for (int i = 0; i < questionaires22.size(); i++) {
            if (questionaires22.get(i).getPr() == 3) {
                questionaires33.add(questionaires22.get(i));
            }
            ;
        }
        return questionaires33;
    }

    public List<Questionaire> findAllTechQuestions() {
        List<Questionaire> questionaires22 = findAllQuestions();
        List<Questionaire> questionaires33 = new ArrayList<Questionaire>();
        for (int i = 0; i < questionaires22.size(); i++) {
            if (questionaires22.get(i).getPr() == 4) {
                questionaires33.add(questionaires22.get(i));
            }
            ;
        }
        return questionaires33;
    }

    private void Confirm() {

        final Dialog dialog = new Dialog(this);
        ToastHelper.show("If Yes you will lose the Current Progress");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.areyousure);
        Button bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        TextView bt_cancel = (TextView) dialog.findViewById(R.id.bt_cancel);
        final Intent intent = new Intent(this, Mode.class);
        bt_ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                cancel();
                ToastHelper.show("Current Progress Lost");
                startActivity(intent);
                finish();


                dialog.dismiss();
            }
        });

        bt_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private void displayAllTechQuestions() {
        start();

        if (l < questionaires4.size()) {

            e = questionaires4.get(l).getId();
            QS = questionaires4.get(l).getQs();
            A = questionaires4.get(l).getA();
            B = questionaires4.get(l).getB();
            C = questionaires4.get(l).getC();
            D = questionaires4.get(l).getD();

            arrayList = new ArrayList();
            arrayList.add(A);
            arrayList.add(B);
            arrayList.add(C);
            arrayList.add(D);
            Collections.shuffle(arrayList);

            String TS = questionaires4.get(l).getTa();
            ques = new Questionaire(e, QS, TS, 4, 10, A, B, C, D);
            l++;
            common();

        } else {
            cancel();

            startActivity(new Intent(this, Congratsmain.class));

            finish();
        }

    }

    private void displayAllgkQuestions() {
        start();

        if (k < questionaires3.size()) {

            e = questionaires3.get(k).getId();
            QS = questionaires3.get(k).getQs();
            A = questionaires3.get(k).getA();
            B = questionaires3.get(k).getB();
            C = questionaires3.get(k).getC();
            D = questionaires3.get(k).getD();

            arrayList = new ArrayList();
            arrayList.add(A);
            arrayList.add(B);
            arrayList.add(C);
            arrayList.add(D);
            Collections.shuffle(arrayList);


            String TS = questionaires3.get(k).getTa();
            ques = new Questionaire(e, QS, TS, 3, 10, A, B, C, D);
            k++;
            common();
            if (k == 11) {
                if (tech == 0) {
                    tech = 1;
                    ToastHelper.show("Technology Has Been Unlocked");
                }
            }

        } else {
            cancel();

            startActivity(new Intent(this, Congratsmain.class));

            finish();
        }

    }

    private void displayAllmathsQuestions() {
        start();

        if (j < questionaires2.size()) {

            e = questionaires2.get(j).getId();
            QS = questionaires2.get(j).getQs();
            A = questionaires2.get(j).getA();
            B = questionaires2.get(j).getB();
            C = questionaires2.get(j).getC();
            D = questionaires2.get(j).getD();

            arrayList = new ArrayList();
            arrayList.add(A);
            arrayList.add(B);
            arrayList.add(C);
            arrayList.add(D);
            Collections.shuffle(arrayList);


            String TS = questionaires2.get(j).getTa();
            ques = new Questionaire(e, QS, TS, 2, 10, A, B, C, D);
            j++;
            common();
            if (j == 11) {
                if (gk == 0) {
                    gk = 1;
                    ToastHelper.show("Gk Has Been Unlocked");
                }
            }

        } else {
            cancel();

            startActivity(new Intent(this, Congratsmain.class));

            finish();
        }

    }

    public void displayAllscienceQuestions() {
        start();


        if (i < questionaires1.size()) {

            e = questionaires1.get(i).getId();


            QS = questionaires1.get(i).getQs();

            A = questionaires1.get(i).getA();
            B = questionaires1.get(i).getB();
            C = questionaires1.get(i).getC();
            D = questionaires1.get(i).getD();

            arrayList = new ArrayList();
            arrayList.add(A);
            arrayList.add(B);
            arrayList.add(C);
            arrayList.add(D);
            Collections.shuffle(arrayList);

            String TS = questionaires1.get(i).getTa();

            ques = new Questionaire(e, QS, TS, 1, 10, A, B, C, D);

            i++;


            common();
            if (i == 11) {
                if (maths == 0) {
                    maths = 1;
                    ToastHelper.show("Maths Has Been Unlocked");
                }
            }


        } else

        {
            cancel();

            startActivity(new Intent(this, Congratsmain.class));
            finish();
        }

    }

    public void common() {

        opta.setVisibility(View.VISIBLE);
        optb.setVisibility(View.VISIBLE);
        optc.setVisibility(View.VISIBLE);
        optd.setVisibility(View.VISIBLE);
        aa = 0;
        bb = 0;
        cc = 0;
        dd = 0;


        opta.setBackgroundResource(R.drawable.s2btn);
        optb.setBackgroundResource(R.drawable.s2btn);
        optc.setBackgroundResource(R.drawable.s2btn);
        optd.setBackgroundResource(R.drawable.s2btn);

        qs.setText(QS);

        opta.setText((CharSequence) arrayList.get(0));

        optb.setText((CharSequence) arrayList.get(1));

        optc.setText((CharSequence) arrayList.get(2));

        optd.setText((CharSequence) arrayList.get(3));


        opta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                opta.setBackgroundResource(R.drawable.s3btn);
                optb.setBackgroundResource(R.drawable.s2btn);
                optc.setBackgroundResource(R.drawable.s2btn);
                optd.setBackgroundResource(R.drawable.s2btn);

                aa = 1;
                bb = 0;
                cc = 0;
                dd = 0;
            }
        });

        optb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                opta.setBackgroundResource(R.drawable.s2btn);
                optb.setBackgroundResource(R.drawable.s3btn);
                optc.setBackgroundResource(R.drawable.s2btn);
                optd.setBackgroundResource(R.drawable.s2btn);

                aa = 0;
                bb = 1;
                cc = 0;
                dd = 0;
            }
        });

        optc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                opta.setBackgroundResource(R.drawable.s2btn);
                optb.setBackgroundResource(R.drawable.s2btn);
                optc.setBackgroundResource(R.drawable.s3btn);
                optd.setBackgroundResource(R.drawable.s2btn);

                aa = 0;
                bb = 0;
                cc = 1;
                dd = 0;
            }
        });
        optd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                opta.setBackgroundResource(R.drawable.s2btn);
                optb.setBackgroundResource(R.drawable.s2btn);
                optc.setBackgroundResource(R.drawable.s2btn);
                optd.setBackgroundResource(R.drawable.s3btn);

                aa = 0;
                bb = 0;
                cc = 0;
                dd = 1;
            }
        });
        LF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t1.stop();
                LifeLine(e);

            }
        });
        NEXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                t1.stop();
                if (aa == 1) {
                    option("a", e);


                } else if (bb == 1) {
                    option("b", e);

                } else if (cc == 1) {
                    option("c", e);

                } else if (dd == 1) {
                    option("d", e);

                } else {
                    ToastHelper.show("Please select a choice");
                }


            }
        });


    }


    public void option(String opt, String f) {
        cancel();
        char option = opt.charAt(0);

        switch (option) {
            case 'a':


                if (ques.getTa().equalsIgnoreCase(opta.getText().toString())) {
                    qsnum++;
                    stnms = String.valueOf(qsnum);
                    num.setText(stnms + ")");
                    src = src + ques.getScr();

                    qstn();

                } else

                {


                    gameOver();


                }

                break;
            case 'b':
                if (ques.getTa().equalsIgnoreCase(optb.getText().toString())) {
                    qsnum++;
                    stnms = String.valueOf(qsnum);
                    num.setText(stnms + ")");
                    src = src + ques.getScr();
                    qstn();

                } else {

                    gameOver();

                }
                break;
            case 'c':
                if (ques.getTa().equalsIgnoreCase(optc.getText().toString())) {
                    qsnum++;
                    stnms = String.valueOf(qsnum);
                    num.setText(stnms + ")");
                    src = src + ques.getScr();

                    qstn();
                } else {

                    gameOver();

                }

                break;
            case 'd':
                if (ques.getTa().equalsIgnoreCase(optd.getText().toString())) {
                    qsnum++;
                    stnms = String.valueOf(qsnum);
                    num.setText(stnms + ")");
                    src = src + ques.getScr();
                    qstn();

                } else {

                    gameOver();

                }

                break;


            default:

                break;
        }
        scrs.setText(src + "");
    }

    private void LifeLine(String i) {


        final Dialog dialog = new Dialog(this);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.lifeline);
        Button life1 = (Button) dialog.findViewById(R.id.lf1);
        Button life2 = (Button) dialog.findViewById(R.id.lf2);

        life1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                int k = 1;
                if (k != one) {
                    if (opta.getText().toString()
                            .equals(ques.getTa())) {
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);

                    } else if (optb.getText().toString()
                            .equals(ques.getTa())) {
                        optd.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);

                    } else if (optc.getText().toString()
                            .equals(ques.getTa())) {
                        opta.setVisibility(View.GONE);
                        optb.setVisibility(View.GONE);

                    } else if (optd.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);

                    }
                    one = 1;
                    dialog.dismiss();
                } else {
                    ToastHelper.show("Life line Used Once");
                    dialog.dismiss();
                }
            }
        });

        life2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                int k = 1;
                if (k != two) {

                    if (opta.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optb.getText().toString()
                            .equals(ques.getTa())) {
                        opta.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optc.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);
                        optd.setVisibility(View.GONE);
                    } else if (optd.getText().toString()
                            .equals(ques.getTa())) {
                        optb.setVisibility(View.GONE);
                        optc.setVisibility(View.GONE);
                        opta.setVisibility(View.GONE);
                    }
                    two = 1;

                    dialog.dismiss();
                } else {
                    ToastHelper.show("Life line Used Once");
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    public void gameOver() {
        Intent intent = new Intent(this, Gameovermain.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

        t1.stop();
        Confirm();

    }


}


