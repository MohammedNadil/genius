package com.nr.genius.ui.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.nr.genius.R;
import com.nr.genius.ui.Dialog.ToastHelper;

import static com.nr.genius.ui.activity.Ques.gk;
import static com.nr.genius.ui.activity.Ques.maths;
import static com.nr.genius.ui.activity.Ques.tech;

/**
 * Created by Nadil on 14-11-2016.
 */

public class Mode extends AppCompatActivity {
    public static int aaaa = 0;
    public static int bbbb = 0;
    public static int cccc = 0;
    public static int dddd = 0;

    Button b1;
    Button b2;
    Button b3;
    Button b4;

    Animation myAnim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gamemenu);
        b1 = (Button) findViewById(R.id.science);
        b2 = (Button) findViewById(R.id.maths);
        b3 = (Button) findViewById(R.id.gk);
        b4 = (Button) findViewById(R.id.history);

        myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        changeStatusBarColor();
        choose_choice();
    }

    private void choose_choice() {
        final Intent intent = new Intent(this, Ques.class);
        aaaa = 0;
        bbbb = 0;
        cccc = 0;
        dddd = 0;

        b2.setBackgroundResource(R.drawable.s2btn);
        b3.setBackgroundResource(R.drawable.s2btn);
        b4.setBackgroundResource(R.drawable.s2btn);

//        b2.setBackgroundColor(Color.parseColor("#400d5f6b"));
//        b3.setBackgroundColor(Color.parseColor("#400d5f6b"));
//        b4.setBackgroundColor(Color.parseColor("#400d5f6b"));
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                b1.startAnimation(myAnim);
                myAnim.setDuration(10);
                aaaa = 1;
                bbbb = 0;
                cccc = 0;
                dddd = 0;
                startActivity(intent);
                finish();
            }
        });
        if (maths == 1) {
            b2.setBackgroundResource(R.drawable.s1btn);


            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b2.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    aaaa = 0;
                    bbbb = 1;
                    cccc = 0;
                    dddd = 0;
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            b2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b2.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    ToastHelper.show("Get 100 score in science to Unlock");
                }
            });
        }
        if (gk == 1) {
            b3.setBackgroundResource(R.drawable.s1btn);
            b3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b3.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    aaaa = 0;
                    bbbb = 0;
                    cccc = 1;
                    dddd = 0;
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            b3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b3.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    ToastHelper.show("Get 100 score in Maths to Unlock");
                }
            });
        }

        if (tech == 1) {
            b4.setBackgroundResource(R.drawable.s1btn);
            b4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b4.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    aaaa = 0;
                    bbbb = 0;
                    cccc = 0;
                    dddd = 1;
                    startActivity(intent);
                    finish();
                }
            });
        } else {
            b4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    b4.startAnimation(myAnim);
                    myAnim.setDuration(10);
                    ToastHelper.show("Get 100 score in GK to Unlock");
                }
            });
        }
    }


    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, Menu.class);
        startActivity(intent);
        finish();
    }
}
