package com.nr.genius.ui.adapter.holder;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nr.genius.R;
import com.nr.genius.constants.Constants;
import com.nr.genius.data.DataProcessController;
import com.nr.genius.domain.Questionaire;
import com.nr.genius.ui.Dialog.Dialog_Update;
import com.nr.genius.ui.adapter.listener.AdapterDAO;

/**
 * Holder User help to easily  handling view in recycler view
 *
 * @author dev.Cobb
 * @version 1.0
 * @since 8 oct 2016
 */
public class Holder_Ques extends RecyclerView.ViewHolder  {

    /**
     * The User name.
     */
    public TextView tv_UserName;
    /**
     * The User phone.
     */
    public TextView tv_UserPhone;
    /**
     * The User option.
     */
    public ImageView img_UserOption;

    /**
     * Instantiates a new Holder user.
     *
     * @param itemView the item view
     */
    public Holder_Ques(View itemView, final Context context , final AdapterDAO adapterDAO) {
        super(itemView);


        tv_UserName=(TextView) itemView.findViewById(R.id.tv_holder_user_name);
        tv_UserPhone=(TextView) itemView.findViewById(R.id.tv_holder_user_phone);
        img_UserOption=(ImageView)itemView.findViewById(R.id.img_holder_user_option);



        img_UserOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                final Questionaire questionaire=(Questionaire) view.getTag();

                PopupMenu popupMenu=new PopupMenu(context,img_UserOption);
                popupMenu.getMenuInflater().inflate(R.menu.menu_main,popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.delete:
                                DataProcessController.getDataProcessController().getQuestionTable().deleteSelectedItem(questionaire.getId(), Constants.TABLE_QUESTION);
                                Constants.Question_Item_Position=getAdapterPosition();
                                adapterDAO.notifyChange(Constants.DELETE);
                                break;
                            case R.id.update:


                              final Dialog_Update dialog_update=new Dialog_Update(context,questionaire,adapterDAO);
                                dialog_update.show();
                                break;
                        }

                        return false;
                    }
                });


                popupMenu.show();}
        });
    }
}
